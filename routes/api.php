<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('crudaction', 'CRUDAction');

Route::name("limite-limite.")
    ->prefix("/limite-limite/{game_id}/")
    ->group(function () {

        Route::get("/get-card/responses/{amount?}", "LimiteLimiteAPI@getResponseCards")->name("get-response-cards");
        Route::get("/get-card/calls/{amount?}", "LimiteLimiteAPI@getCallCards")->name("get-call-cards");
        Route::put("/play-card/", "LimiteLimiteAPI@playCard")->name("play-card");
        Route::get("/check-round/", "LimiteLimiteAPI@checkRound")->name("check-round");
        Route::get("/retrieve-last/", "LimiteLimiteAPI@retrieveLast")->name("retrieve-last");
        Route::put("/restart", "LimiteLimiteAPI@restart")->name("restart");
        Route::put("/leave", "LimiteLimiteAPI@leave")->name('leave');

        Route::get("/status", "LimiteLimiteAPI@status")->name("status");

    });
