<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "Games\LimiteLimiteController@home")->name('home');
Route::post('/', "HomeController@setUsername")->name('set-username');

Route::get("/early-access", "HomeController@earlyAccess")->name("early-access");
Route::post("/early-access", "HomeController@checkEarlyAccessToken");

Route::namespace("Games\\")
    ->middleware('earlyAccess')
    ->group(function () {

        /**
         * ------------------------------------------------------
         *              Limit-Limit routes
         * ------------------------------------------------------
         *
         */
        Route::prefix("/limite-limite")
            ->name("limite-limite.")
            ->group(function () {

                Route::get("/", "LimiteLimiteController@home")->name('home');
                Route::post("/", "LimiteLimiteController@store")->name("store");
                Route::get("/create", "LimiteLimiteController@create")->name("create");
                Route::get("/{token}", "LimiteLimiteController@show")->name("show");
                Route::get("/{token?}/join", "LimiteLimiteController@join")->name("join");
                Route::post("/{token}/join", "LimiteLimiteController@joinRequest")->name("join-request");

            });

    });
