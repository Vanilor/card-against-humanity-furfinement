function notify(level, icon, message) {

    let delay = 2500;

    if (level === "danger")
        delay = -1;

    $.notify({
        icon: icon,
        message: message,
    }, {

        type: level,
        allow_dismiss: true,
        newest_on_top: true,
        placement: {
            from: "top",
            align: "right"
        },
        delay: delay,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });

}

window.selected = undefined;
window.isHandShow = 0;

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function animateElement(elementId, animation) {
    let element = document.getElementById(elementId);
    element.classList.add('animated', animation);
    element.addEventListener('animationend', function () {
        element.classList.remove('animated', animation);
    });
}

function animateHandContainer() {
    $("#player-hand-collapse.slideInUp, #player-hand-collapse.visibleSlideOutDown").toggleClass("slideInUp visibleSlideOutDown");
}

function showGameOptions() {
    $(".c-hideable-go").toggleClass("c-hide-go");
}

function showLastRound() {
    $(".c-hideable-lr").toggleClass("c-hide-lr");
}

function selectCard(el) {
    if (window.selected !== undefined) {
        window.selected.classList.remove("selected");
    }

    window.selected = el;
    window.selected.classList.add("selected");
}
