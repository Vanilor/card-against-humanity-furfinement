<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLimiteLimiteSessionsTable extends Migration
{
    private $table;

    public function __construct()
    {
        $this->table = "limite_limite_sessions";
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->string('token')->unique();
            $table->string('name');
            $table->string("password");
            $table->string("host");


            /**
             * current_player_status JSON content:
             *
             * @string  username
             * @integer scores
             * @boolean is_czar
             * @array  current_card
             *
             * {
             *       {
             *          "username" => "Dindon",
             *          "score" => 3,
             *          "is_czar" => 0,
             *          "current_card" => ["Mon super dindon"] || []
             *       }
             * }
             */
            $table->string("current_players_status");
            $table->string('card_sets'); //@array
            $table->binary('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
