<?php

return [

    "game_not_found"    => "The game you looked for was not found. Maybe the token :token is wrong ?",
    "crash"             => "Oh no, the API has crashed. Try your request again or contact a developer"

];
