<?php

return [

    "loading"                   => "Chargement...",
    "please_choose_card"        => "Vous devez choisir une carte avant de jouer",
    "no_choosen_game"           => "Choisissez une partie si vous voulez en rejoindre une :(",
    "username_already_taken"    => "Ce nom d'utilisateur est déjà pris :(",
    "invalid_password"          => "Le mot de passe est invalide :(",
    "welcome_back"              => "Rebonjour ! Nous vous attendions :)",
    "thanks_for_playing"        => "Merci d'avoir joué ! :D",
    "see_last_round"            => "Voir le tour précédent",
    "see_current_round"         => "Voir le jeu actuel",
    "create_game"               => "Créer une partie",
    "playing"                   => "Partie en cours...",
    "game_over"                 => "Partie terminée",
    "error_happened"            => "Une erreur est survenue",
    "bug_report"                => "Report de bug",
    "limite-limite"             => [

        "youre_czard"           => "<b>Vous êtes le Czard</b>",
        "byebye"                => "Vous avez quitté la partie, à la prochaine !",

    ],

];
