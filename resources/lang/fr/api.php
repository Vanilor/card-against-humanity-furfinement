<?php

return [

    "game_not_found"    => "Nous n'avons pas trouvé la partie que vous cherchez, peut-être que la clé :token est fausse ?",
    "crash"             => "L'API a plantée. :( <br /> Relancez votre requête ou contactez un développeur en lui donnant les informations suivantes : <br /> :datas.",

    "limite_limite"     => [

        "invalid_card_amount"       => "Le nombre de cartes demandée est inférieur ou égal à 0. Si tu ne veux pas de cartes, n'en demande pas !",
        "invalid_card_type"         => "La carte demandée est inconnue. Es-tu sûr de ne pas avoir demandé un dinosaure ?",
        "invalid_parameters"        => "Les paramètres entrés ne sont pas reconnus. Vous vous êtes perdu ?",
        "dont_own_card"             => "Vous ne possédez pas cette carte. N'essayez pas de voler celles des autres !",
        "cant_play"                 => "Vous ne pouvez pas jouer pendant ce tour. Vous ne seriez pas le czard par hasard ?",
        "didnt_play"                => "Vous n'avez pas encore joué pendant cette manche.",
        "already_played"            => "Vous avez déjà joué une carte. Cliquez sur le bouton \"Récupérer ma carte\" si vous avez fais une erreur :)",
        "already_owns_card"         => "Vous avez déjà cette carte dans votre main. Des soucis de vision ?",
        "already_retrieved"         => "Vous avez déjà récupéré votre carte.",
        "missing_players"           => "Il manque encore :missing joueurs avant de pouvoir jouer",
        "missing_players_czard"     => ":missing n'ont pas encore joués",
        "insufficient_permissions"  => "Seul l'hôte de la partie peut faire cela !",
        "round_over"                => "Le tour est terminé, vous ne pouvez plus jouer !",
        "cant_quit_host"            => "Vous ne pouvez pas quitter la partie tant que vous en êtes l'hôte",
        "cant_quit_czard"            => "Vous ne pouvez pas quitter la partie tant que vous en êtes le Czard",

    ],

];
