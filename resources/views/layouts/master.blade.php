<!DOCTYPE html>
<html lang="fr">

    <head>

        @hasSection("title")
            <title>@yield("title") • {{ app_name() }}</title>
        @else
            <title>{{ app_name() }}</title>
        @endif

        @include("layouts.head")
        @yield("styles")

    </head>

    <body>

        <!-- Body Container -->
        <div class="content">
            @include("layouts.nav-top")

            <!-- Page Content -->
            <div @hasSection("container") class="@yield("container")" @else class="container" @endif">

                @yield("content")

            </div>
        </div>

        @include("layouts.footer")
        @include("layouts.scripts")
        @include("layouts.notification")
        @yield("scripts")
    </body>

</html>
