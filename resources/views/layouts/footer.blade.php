<!-- Footer -->
<footer class="footer mt-4 pt-4 bg-secondary">

    <!-- Upper Footer -->
    <div class="container text-center">
        <div class="row">

            <!-- Footer Text -->
            <div class="col-12 col-md-6 text-left">
                <h5 class="text-uppercase">{{ app_name() }}</h5>
                <p>{{ trans("strings.created_by") }} <a href="https://twitter.com/AddranorDev" target="_blank">Adrannor</a> {{ trans("strings.and") }} <a href="https://twitter.com/VanilorDev" target="_blank">Vanilor</a></p>
            </div>

            <!-- Links -->
            <div class="col-6 col-md-3">
                <h5 class="text-uppercase">Ressources</h5>
                <ul class="list-unstyled">
                    <li><a href="https://cardmanager.vanilor.info">CardManager</a></li>
                </ul>
            </div>

            <!-- Links -->
            <div class="col-6 col-md-3">
                <h5 class="text-uppercase">Contact</h5>
                <ul class="list-unstyled">
                    <li><a href="https://gitlab.com/Vanilor/card-against-humanity-furfinement/-/issues">{{ trans("app.bug_report") }}</a></li>
                    <li>Discord: Vanilor#9180</li>
                </ul>
            </div>

        </div>
    </div>

    <hr class="m-0">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
        <a href="https://mdbootstrap.com/"> MDBootstrap.com</a>
    </div>

</footer>
