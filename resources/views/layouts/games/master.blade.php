<!DOCTYPE html>
<html lang="fr">

    <head>

        @hasSection("title")
            <title>@yield("title") • {{ app_name() }}</title>
        @else
            <title>{{ app_name() }}</title>
        @endif

        @include("layouts.head")
        @yield("styles")

    </head>

    <body>

        @include("layouts.nav-top")

        <div class="container-fluid">
            <div id="game-window" class="row">

                @yield("content")

            </div>
        </div>

        @include("layouts.scripts")
        @include("layouts.notification")
        @yield("scripts")
    </body>

</html>
