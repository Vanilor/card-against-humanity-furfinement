<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="author" content="Addranor, Vanilor">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="shortcut icon" href="{{ asset("favicon.png")}}" type="image/png">

<!-- Online CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.4.1/yeti/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.4.1/darkly/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

<!-- Local CSS -->
<link rel="stylesheet" href="{{ asset("css/app.css") }}">
<link rel="stylesheet" href="{{ asset("css/light.css")}}">
<link rel="stylesheet" href="{{ asset("css/dark.css")}}">
<link rel="stylesheet" href="{{ asset("css/debug.css")}}">
