<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#top-nav" aria-controls="top-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="tpo-nav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('home') }}">
                    Accueil
                </a>
            </li>
        </ul>

        @if(!session()->has("username"))
        <form action="{{ route('set-username') }}" method="POST" class="col-4">

            <div class="input-group">
                <input type="text" class="form-control" name="username" placeholder="Choisir un nom d'utilisateur">
                <div class="input-group-append">
                    <input type="submit" class="btn btn-primary" value="Choisir" />
                </div>
            </div>
            @csrf
        </form>

        @else
            <span class="text-light">Tu as le pseudo <b>{{ session()->get("username") }}</b></span>
        @endif
    </div>
</nav>
