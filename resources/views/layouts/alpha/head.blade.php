<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Addranor, Vanilor">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset("css/animate.min.css") }}" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset("css/app.css") }}" rel="stylesheet" />
