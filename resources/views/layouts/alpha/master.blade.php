<!DOCTYPE html>
<html lang="en">

    <head>

        @hasSection('title')
            <title>{{ app_name() }} - @yield("title")</title>
        @else
            <title>{{ app_name() }}</title>
        @endif

        @include('layouts.head')
        @yield('styles')
    </head>

    <body>

        @include('layouts.nav')
        <div class="container" style="margin-top: 15px">
            @yield("content")
        </div>

        @include("layouts.scripts")
        @include("layouts.notification")
        @yield('scripts')
    </body>

</html>
