<script type="text/javascript">
    @foreach (session('flash_notification', collect())->toArray() as $message)

        notify("{{ $message['level'] }}",
                @if($message['level'] === "danger") 'fas fa-times', @elseif($message['level'] === "success") 'fas fa-check', @elseif($message["level"] === "info") 'fas fa-info-circle', @else 'fas fa-exclamation-triangle', @endif
                "{{ $message['message'] }}");

    @endforeach
</script>

@php(session()->forget('flash_notification'))
