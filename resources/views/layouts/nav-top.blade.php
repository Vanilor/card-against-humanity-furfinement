<!-- Navigation Bar -->
<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark justify-content-between">
    <div class="container-fluid">

        <a class="navbar-brand d-none d-sm-block" href="#"><img src="{{ asset("images/logo.png") }}" alt="{{ app_name() }}"></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link" href="{{ route("limite-limite.create") }}">Créer une partie</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route("limite-limite.home")}}">Liste des parties</a></li>
            </ul>

            <ul class="navbar-nav">
                <li class="dropdown">
                    <a href="" onclick="animateElement('login_menu', 'fadeInLeft')" id="login_button"
                       class="dropdown-toggle nav-link" data-toggle="dropdown" title="User" aria-expanded="true">
                        Se connecter
                    </a>
                    <div id="login_menu" class="dropdown-menu dropdown-menu-right fast">

                        <!-- if LOGGED_IN -> HIDE THIS -->
                        <!-- <h6 class="dropdown-header text-center">Gestion de compte</h6>

                    <div class="img-container"><img src="http://placehold.it/100x100" alt=""></div>
                    <h6 class="text-center mx-3 text-light">Nom d'utilisateur</h6>
                    <h6 class="text-center mx-3 text-light">Adresse@Mail</h6>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item px-3 pt-2" href="#"><i class="fas c-fas fa-user-circle"></i> Profil</a>
                    <a class="dropdown-item px-3 pt-2" href="#"><i class="fas c-fas fa-window-restore"></i> Decks favoris</a>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item px-3 pt-2" href="#"><i class="fas c-fas fa-power-off"></i> Déconnexion</a> -->
                        <!-- endif -->

                        <!-- if LOGGED_IN -> SHOW THIS -->
                        <h6 class="dropdown-header text-center">Connexion</h6>
                        <div class="dropdown-divider mb-3"></div>

                        <form class="px-3">
                            <div class="form-group">
                                <label class="text-muted" for="usernameInput">Nom d'utilisateur</label>
                                <input type="text" class="form-control" id="usernameInput" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label class="text-muted" for="passwordInput">Mot de Passe</label>
                                <input type="password" class="form-control" id="passwordInput" placeholder="Password">
                            </div>

                            <fieldset class="form-group">
                                <div class="form-check text-muted">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="" checked="">
                                        Se souvenir de moi ?
                                    </label>
                                </div>
                            </fieldset>

                            <div class="form-group">
                                <input type="button" class="btn btn-block btn-primary mt-1" value="Connexion">
                            </div>
                        </form>

                        <div class="text-center w-100">
                            <div class="badge font-weight-light">
                                <a class="text-muted" href="#">Mot de passe oublié ?</a>
                                <span class="text-muted">-</span>
                                <a class="text-muted" href="#">Créer un compte</a>
                            </div>
                        </div>
                        <!-- endif -->

                    </div>
                </li>
            </ul>

        </div>

    </div>
</nav>
