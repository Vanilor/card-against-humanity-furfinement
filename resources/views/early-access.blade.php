@extends('layouts.master')

@section('content')

    <h1 class="mt-5 text-center">CARDGAME - ACCÈS ANTICIPÉ</h1>

    <div class="row justify-content-center">
        <div class="col-8">
        <form action="{{ route('early-access') }}" method="POST" style="margin-top: 40px">

            <div class="input-group col-8">
                <input type="text" class="form-control" id="token" name="token" placeholder="Entrez votre token d'accès anticipé...">
                <div class="input-group-append">
                    <input type="submit" class="btn btn-primary" value="Envoyer"/>
                </div>
            </div>
            @csrf
        </form>
        </div>
    </div>

@endsection
