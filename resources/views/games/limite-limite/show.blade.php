@extends("layouts.games.master")

@section("title", "Partie")

@section("content")

    <div id="left-side">

        <!-- Toggle Options -->
        <div id="options-toggler">
            <div onclick="showGameOptions()"><i class="fas fa-cog"></i> Options</div>
        </div>

        <!-- Current Round - Call Card -->
        <div class="call-card-container c-hideable-lr c-hideable-go">
            <div class="call-card">
                <div class="card-text">
                    {{ trans("app.loading") }}...
                </div>
            </div>
            <span id="buttons">
                <button class="btn btn-sm btn-block btn-primary" onclick="sendCard()" id="btn-send">Confirmer</button>
                <button class="btn btn-sm btn-block btn-primary d-none" onclick="retrieveLastPlayedCard()"
                        id="btn-retrieve">Récupérer</button>
            </span>
        </div>

        <!-- Last Round - Call Card -->
        <div class="call-card-container c-hideable-lr c-hideable-go d-none">
            <div class="call-card">
                <div class="card-text">
                    <span class="call-card-placeholder"></span> Ceci est le contenu de la carte précédente !
                </div>
            </div>
        </div>

        <!-- Options -->
        <div class="game-button-container c-hideable-go c-hide-go">
            <a class="btn btn-sm btn-block btn-secondary" id="restart-btn">Redémarrer la partie</a>
            <a class="btn btn-sm btn-block btn-secondary" id="stop-btn">Arrêter la partie</a>
            <a class="btn btn-sm btn-block btn-secondary" id="start-btn">Démarrer la partie</a>
            <a class="btn btn-sm btn-block btn-secondary" id="leave-btn">Quitter la partie</a>
            <a class="btn btn-sm btn-block btn-secondary" id="save-btn">Sauvegarder les règles</a>
        </div>

    </div>

    <div id="right-side">

        <!-- Toggle Last Round -->
        <div id="last-round-toggler">
            <div class="c-hideable-lr">
                <button id="lastRound-btn" onclick="showLastRound()" class="c-hideable-lr">
                    <i class="fas fa-reply"></i> {{ trans("app.see_last_round") }}
                </button>
            </div>
        </div>

        <!-- Current Round Tab -->
        <div id="played-cards" class="c-hideable-lr c-hideable-go">

        </div>

        <!-- Last Round Tab -->
        <div id="played-cards-last" class="c-hideable-lr c-hideable-go d-none">


        </div>

        <!-- Options Tab -->
        <div id="game-options" class="c-hideable-go c-hide-go">
            <table id="game-options-table">
                <thead>
                <tr>
                    <td></td>
                    <td class="text-left">Options de la partie</td>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td><input class="form-control form-control-sm" type="text" id="rule-1"></td>
                    <td class="text-left"><label for="rule-1">Text Input Option</label></td>
                </tr>

                <tr>
                    <td>
                        <select class="form-control form-control-sm" name="pets" id="rule-2">
                            <optgroup label="Title">
                                <option value="1">Value 1</option>
                                <option value="2">Value 2</option>
                                <option value="3">Value 3</option>
                            </optgroup>
                        </select>
                    </td>
                    <td class="text-left"><label for="rule-2">Select Input Option</label></td>
                </tr>

                <tr>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="" id="rule-3">
                        </div>
                    </td>
                    <td class="text-left"><label for="rule-3">Checkbox Input Option</label></td>
                </tr>

                <tr>
                    <td>
                        <div class="form-check form-check-inline mr-2">
                            <input class="form-check-input" type="radio" name="example_radio" id="rule-4-1" value="1"
                                   checked>
                            <label for="rule-4-1" class="form-check-label">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="example_radio" id="rule-4-2" value="0">
                            <label for="rule-4-2" class="form-check-label">No</label>
                        </div>
                    </td>
                    <td class="text-left"><label class="form-check-label">Radio Input Option</label></td>
                </tr>

                </tbody>
            </table>
        </div>

        <!-- Player Hand -->
        <div id="player-hand-fixed" class="c-hideable-lr c-hideable-go">
            <div id="player-hand-collapse" class="animated slideInUp">

                <!-- Bouton pour la main du joueur -->
                <div onclick="animateHandContainer()" id="player-hand-trigger" type="button">
                    <i class="fas fa-bars"></i> Votre main
                </div>

                <!-- Main du joueur -->
                <div id="player-hand-container">
                    <div id="player-hand-cards">

                        <div onclick="selectCard(this)" class="response-card-container selectable">
                            <div class="response-card">
                                <div class="card-text font-weight-bold">Lorem ipsum dolor sit amet consectetur
                                    adipisicing elit. Odit iusto, minus alias magnam culpa necessitatibus ut
                                    consequuntur quam obcaecati non nemo est qui, nulla facilis pariatur iure ?
                                </div>
                            </div>
                        </div>

                        <div onclick="selectCard(this)" class="response-card-container selectable">
                            <div class="response-card">
                                <div class="card-text font-weight-bold relative">
                                    <div class="blank-card"></div>
                                    <i class="fas fa-pencil-alt blank-card-logo"></i></div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Score Board -->
        <div id="scoreboard-container">

            <table class="w-100">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-left">Joueur</th>
                    <th class="text-center">Score</th>
                </tr>
                </thead>
                <tbody>

                <!-- Is host -->
                <!-- <i class="fas fa-home"></i> -->

                <!-- Joueur étant le card czar -->
                <tr>
                    <td class="text-center"><i class="fas fa-crown"></i></td>
                    <td class="text-left"></td>
                    <td class="text-left">#NOM</td>
                    <td class="text-center">#SCORE</td>
                </tr>

                <!-- Joueur ayant joué -->
                <tr>
                    <td class="text-center"><i class="fas fa-check"></i></td>
                    <td class="text-left"></td>
                    <td class="text-left">#NOM</td>
                    <td class="text-center">#SCORE</td>
                </tr>

                <!-- Joueur n'ayant pas encore joué -->
                <tr>
                    <td class="text-center"><i class="fas fa-times"></i></td>
                    <td class="text-left"></td>
                    <td class="text-left">#NOM</td>
                    <td class="text-center">#SCORE</td>
                </tr>

                <!-- Joueur hôte de la manche -->
                <tr>
                    <td class="text-center"><i class="fas fa-times"></i></td>
                    <td class="text-left"><i class="fas fa-home"></i></td>
                    <td class="text-left">#NOM</td>
                    <td class="text-center">#SCORE</td>
                </tr>

                <!-- Joueur gagnant de la manche -->
                <tr class="player-win">
                    <td class="text-center"><i class="fas fa-times"></i></td>
                    <td class="text-left"></td>
                    <td class="text-left">#NOM</td>
                    <td class="text-center">#SCORE</td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>

@endsection

@section("scripts")

    <script type="text/javascript">

        /**
         * @TODO Add player_api_token and JSON datatype in Ajax setup (check all API returns before, do this during code refactoring)
         */

        window.game = Object.create({

            selected: undefined,
            checkGame: setInterval(checkRound, 1500),
            status: undefined,
            didYouPlay: false,
            isGameOver: 0,
            lastWinner: undefined,
            lastCallCard: undefined,
            currentCallCard: undefined,

        });

        const player_api_token = "{{ session()->get('player_api_token') }}";

        /*
   * Roles identifiers
   */
        const IDENTIFIER_HOST           = "<i class=\"fas fa-home\"></i>";
        const IDENTIFIER_CZARD          = "<i class=\"fas fa-crown\"></i>";
        const IDENTIFIER_HAS_PLAYED     = "<i class='fas fa-check '></i>";
        const IDENTIFIER_HAS_NOT_PLAYED = "<i class='fas fa-times'></i>";
        const IDENTIFIER_WINNER         = "<i class='fas fa-crown '></i>";
        const IDENTIFIER_HONORABLE      = "<i class='fas fa-plus'></i>";

        let callCards       = $(".call-card>.card-text");
        let responseCards   = $("#player-hand-cards");
        let info            = $("#infos");
        let playersStatus   = $("#scoreboard-container>table>tbody");
        let buttons         = $("#buttons");
        let lastRound       = $("#played-cards-last");
        let currentRound    = $("#played-cards");
        let playedCardsNb   = $("#played-cards-nb");
        let myCards         = $("#played-cards");
        let seeLastRoundBtn = $("#lastRound-btn");
        let sendBtn;
        let retrieveBtn;

        $(document).ready(() => {

            sendBtn = document.getElementById("btn-send");
            retrieveBtn = document.getElementById("btn-retrieve");

        });

        //Add an unselect feature
        $("body").on('click', function (event) {

            if((event.target.nodeName === "BUTTON") || event.target.hasAttribute("onclick"))
                return;

            if(window.game.status !== "round_over") {

                show(sendBtn);
                show(retrieveBtn);

                if (!window.game.selected && window.game.didYouPlay)
                    vanish(sendBtn);
                else {

                    vanish(retrieveBtn);
                    enable(sendBtn);
                }

            }

            if (window.game.selected && !event.target.classList.contains("card-text")) {

                window.game.selected.classList.remove('selected');
                window.game.selected = undefined;

            }
        });

        function sendCard() {

            if (window.game.selected === undefined) {
                notify("warning", 'fas fa-exclamation-circle', "{{ trans("app.please_choose_card") }}");
                return;
            }

            disable(sendBtn);
            disable(retrieveBtn);

            $.ajax({

                method: "PUT",
                url: "{{ route("api.limite-limite.play-card", $token) }}",
                data: {
                    player_api_token: player_api_token,
                    card_id: window.game.selected.dataset.id
                },
                datatype: "json",
                success: function (data) {

                    window.game.selected = undefined;
                    enable(retrieveBtn);

                    //You're czard error message
                    if (data.status === "unauthorized") {
                        notify("warning", "fas fa-exclamation-circle", data.message.text);
                        //responseCards.append(getCardDiv(data.message.played_card));
                    }

                    else if (data.status === "czard_choosen") {

                        playersStatus.html(generatePlayersStatus(data));
                        myCards.html(generatePlayedResponses(data));
                        $("[data-ident=" + data.message.winner + "]").addClass("selected");

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message)

                }

            });

            window.game.selected.remove();

        }

        function retrieveLastPlayedCard() {

            window.game.selected = undefined;
            myCards.html("Vous avez joué : ");

            $.ajax({

                method: "GET",
                url: "{{ route("api.limite-limite.retrieve-last", $token) }}",
                data: {
                    player_api_token: player_api_token,
                },
                dataType: "JSON",
                success: function (data) {

                    disable(retrieveBtn);

                    if (data.status === "unauthorized") {
                        notify("warning", "fas fa-exclamation-circle", data.message);
                    } else {

                        for (let el of data.message)
                            responseCards.append(getCardDiv(el.card));

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);

                }

            });

        }

        function checkRound() {

            $.ajax({

                method: "GET",
                url: "{{ route("api.limite-limite.check-round", $token) }}",
                data: {
                    player_api_token: player_api_token
                },
                datatype: "json",
                success: function (data, textStatus, jqHXR) {

                    window.game.status = data.status;
                    window.game.currentCallCard = data.message.call_card;
                    window.game.lastCallCard = data.message.last_round.call_card;

                    show(retrieveBtn);
                    disable(retrieveBtn);
                    show(sendBtn);
                    disable(sendBtn);

                    if (window.game.isGameOver)
                        info.html("");

                    //Displays call card
                    if (data.status !== "game_over")
                        callCards.html(data.message.call_card);

                    playersStatus.html(generatePlayersStatus(data));

                    //If Last round is active
                    if (!lastRound.hasClass("d-none")){

                        activateLastRoundShown();
                    }


                    if (data.status === "not_found") {

                        window.game.location.href = "{{ route("limite-limite.home") }}";

                    }

                    //They are missing players. Everyone's waiting
                    else if (data.status === "waiting") {

                        info.html("");
                        myCards.html("");
                        playedCardsNb.html(data.message.missing);
                        playersStatus.html("");
                        callCards.html("");
                        responseCards.html("");

                    }

                    //The player is czard and waiting for regular players to play
                    else if (data.status === "czard_waiting") {

                        myCards.html("{{ trans("api.limite_limite.missing_players_czard", ["missing" => 0]) }}".replace("0", data.message.missing));
                        responseCards.html("{!! trans("app.limite-limite.youre_czard") !!}");

                        vanish(retrieveBtn);
                        disable(retrieveBtn);
                        vanish(sendBtn);
                        disable(sendBtn);

                    }

                    //No call card send by API in case of round end and player is czard
                    //The player is currently czard and is choosing a response card
                    else if (data.status === "czard_choosing") {


                        if(window.game.selected)
                            enable(sendBtn);
                        else
                            disable(sendBtn);

                        responseCards.html("{!! trans("app.limite-limite.youre_czard") !!}");
                        retrieveBtn.classList.add("d-none");
                        playedCardsNb.html("Nombre de cartes jouées : " + data.message.total);
                        myCards.html(generatePlayedResponses(data));
                    }

                    else if (data.status === "game_over") {

                        responseCards.html("");
                        callCards.html("");
                        info.html("");

                        //Vanish buttons
                        disable(sendBtn);
                        vanish(sendBtn);
                        disable(retrieveBtn);
                        vanish(retrieveBtn);

                        $(".selected").removeClass("selected");

                        if (!(window.game.isGameOver))
                            window.game.isGameOver = 1;

                        if ((data.message.restart_token !== undefined) && (!$("#btn-restart").length)) {
                            buttons.append("<button class='btn btn-sm btn-block btn-primary' id='btn-restart' onclick='restartGame(\"" + data.message.restart_token + "\")'>Relancer une partie</button>");
                        }

                    }

                    //The player is a regular one and is on-game
                    else {

                        enable(sendBtn);

                        //Disable the Retrieve button when the call card change
                        if (data.message.call_card !== callCards.text())
                            disable(retrieveBtn);

                        if ((data.status === "round_over") || (data.status === "czard_choosen")) {

                            myCards.html(generatePlayedResponses(data));
                            vanish(sendBtn);
                            disable(sendBtn);
                            vanish(retrieveBtn);
                            disable(retrieveBtn);
                        }

                        else {

                            let playedData = data.message.response_cards_played.by_me;
                            let playedCards = "";

                            if (playedData != false) {

                                window.game.didYouPlay = true;
                                for (const el of playedData)
                                    playedCards += getCardDiv(el.card, false);

                                //If the player has already played a card but didn't selected another one
                                if(!window.game.selected)
                                    vanish(sendBtn);
                                else
                                    vanish(retrieveBtn);

                                enable(retrieveBtn);

                            }

                            else {

                                window.game.didYouPlay = false;
                                if(!window.game.selected)
                                    disable(sendBtn);

                                vanish(retrieveBtn);
                                disable(retrieveBtn);

                            }

                            playedCardsNb.html("Nombre de cartes jouées : " + data.message.response_cards_played.total);
                            myCards.html("Vous avez joué : <br /><br />" + playedCards);

                        }
                        //Purge current responses cards
                        responseCards.html("");

                        //Display responses cards
                        for (const card of data.message.my_cards) {

                            //Add the text success class to this element if it was previously selected in order to
                            //prevent refresh disappearing

                            let isSelected = ((window.game.selected !== undefined) && (window.game.selected.dataset.id == card.id));

                            let responseStr = "";
                            responseStr += '<div class="response-card-container group-container selectable ' + (isSelected ? "selected" : "") + '" onclick="selectCard(this)" data-id="' + card.id + '">';
                            responseStr += getCardDiv(card, false, []);
                            responseStr += "</div>";

                            responseCards.append(responseStr);
                        }

                    }

                    //Generates last round div
                    lastRound.html("");
                    for (const player of data.message.last_round.players) {

                        if(player.cards === [])
                            continue;

                        let responseStr = "";
                        responseStr += '<div class="response-card-container group-container ' + (player.is_winner ? "selectable selected" : "") + '">';

                        for (const card of player.cards){
                            responseStr += getCardDiv(card, true, {
                                name: player.name,
                                deck: card.deck
                            });
                        }
                        responseStr += "</div>";

                        lastRound.append(responseStr);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);

                }

            });

        }

        function restartGame(token) {

            $.ajax({

                method: "PUT",
                url: "{{ route("api.limite-limite.restart", $token) }}",
                dataType: "json",
                data: {
                    player_api_token: player_api_token,
                    restart_token: token,
                },
                success: function (data) {
                    $("#btn-restart").remove();
                    info.html("");
                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);

                }
            });

        }

        function leave() {

            $.ajax({
                method: "PUT",
                url: "{{ route("api.limite-limite.leave", $token) }}",
                dataType: "json",
                data: {
                    player_api_token: player_api_token,
                },
                success: function () {

                    window.game.location.href = "{{ route("limite-limite.home") }}";

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);
                }
            })

        }

        /*
         * ==============================
         * Utils functions
         * ==============================
         */

        function generatePlayedResponses(data) {

            let str = "";
            let list = "<div>";
            const displayNames = (data.status === "czard_choosen");
            const isCzard = (data.status === "czard_choosing");

            for (const pl of data.message.response_cards_played) {

                let signatureData = {};

                console.log(data.message.winner+" "+pl.ident);

                if (
                    (displayNames && (data.message.winner == pl.ident)) ||
                    ((window.game.selected !== undefined)) && (window.game.selected.dataset.id == pl.response_id)
                ) {

                    str += '<div class="response-card-container ' + ((pl.cards.length > 1) ? "group-container" : "") + ' '+ ((isCzard || displayNames) ? "selectable selected" : "") +'" onclick="selectCard(this)" '+(isCzard ? "data-id=\"" + pl.response_id + "\"" : "") +'>';

                }

                else
                    str += '<div class="response-card-container ' + ((pl.cards.length > 1) ? "group-container" : "") + ' '+ (isCzard ? "selectable" : "") +'" onclick="selectCard(this)" '+ (isCzard ? "data-id=\"" + pl.response_id + "\"" : "") +'>';

                if (displayNames)
                    signatureData.name = pl.name;

                for (const card of pl.cards) {
                    signatureData.deck = card.deck;
                    str += getCardDiv(card, true, signatureData);
                }

                list += str;
            }

            list += "</div>";

            return list;

        }

        function generatePlayersStatus(data) {

            //Display player sidebar
            let currentPlayerStatus = "";
            let lastRoundWinnerIdent = undefined;

            for(const lastRoundPlayer of data.message.last_round.players)
                if(lastRoundPlayer.is_winner == true)
                    lastRoundWinnerIdent = lastRoundPlayer.ident;

            for (const player of data.message.players) {

                if (((data.status === "czard_choosen") || (data.status === "game_over")) &&
                    (player.ident === data.message.winner)) {

                    if (data.status === "czard_choosen")
                        player.score++; //Display updated score instead of old one

                    currentPlayerStatus += '<tr class="player-win-game" data-ident="' + player.ident + '" '+ ((player.ident == lastRoundWinnerIdent) ? "data-lastWinner='1'" : "") +'> \n';
                } else {
                    currentPlayerStatus += '<tr data-ident="' + player.ident + '" '+ ((player.ident == lastRoundWinnerIdent) ? "data-lastWinner='1'" : "") +'> \n';
                }

                if (player.status.is_czard === true)
                    currentPlayerStatus += '<td class="text-center">' + IDENTIFIER_CZARD + '</td>\n';
                else if (player.status.has_played === true)
                    currentPlayerStatus += '<td class="text-center">' + IDENTIFIER_HAS_PLAYED + '</td>\n';
                else
                    currentPlayerStatus += '<td class="text-center">' + IDENTIFIER_HAS_NOT_PLAYED + '</td>\n';

                if (player.status.is_host === true)
                    currentPlayerStatus += '<td class="text-center">' + IDENTIFIER_HOST + '</td>\n';
                else
                    currentPlayerStatus += '<td class="text-left"></td> \n';

                currentPlayerStatus += '<td class="text-left">' + player.name + '</td> \n' +
                    '<td class="text-center">' + player.score + '</td>\n' +
                    '</tr>\n';
            }

            return currentPlayerStatus;
        }

        /**
         * Show last round
         */
        function showLastRound() {

            //Show last round
            if (lastRound.hasClass("d-none")) {

                activateLastRoundShown();
                seeLastRoundBtn.html('<i class="fas fa-share"></i> '+"{{ trans("app.see_current_round") }}");
                currentRound.addClass("d-none");
                lastRound.removeClass("d-none");

            }
            //Show current round
            else {
                desactiveLastRoundShown();
                seeLastRoundBtn.html('<i class="fas fa-reply"></i> ' + "{{ trans("app.see_last_round") }}");
                currentRound.removeClass("d-none");
                lastRound.addClass("d-none"); //Hide last round
            }

        }

        function activateLastRoundShown(){

            if(window.game.lastCallCard)
                callCards.html(window.game.lastCallCard);

            let lastWinner = document.querySelector("[data-lastWinner='1']");
            if(lastWinner)
                lastWinner.classList.add("player-win-game");

        }

        function desactiveLastRoundShown() {

            callCards.html(window.game.currentCallCard);

            let lastWinner = document.querySelector("[data-lastWinner='1']");
            if(lastWinner)
                lastWinner.classList.remove("player-win-game");

        }

        /**
         * Enables an element by removing disabled properties and style
         *
         * @param el
         */
        function enable(el) {

            el.removeAttribute("disabled");
            el.classList.remove("disabled");

        }

        /**
         * Disable an element by adding disabled properties and style
         *
         * @param el
         */
        function disable(el) {
            el.setAttribute("disabled", "disabled");
            el.classList.add("disabled");
        }

        function isEnabled(el){
            return el.hasAttribute("disabled");
        }

        function isVanished(el){

            return el.hasClass("d-none") || el.classList.contains("d-none");

        }

        function vanish(el) {
            el.classList.add("d-none");
        }

        function show(el) {
            el.classList.remove("d-none");
        }

        /**
         *
         * Generates a card div by getting the API content descriptor
         *
         * @param card
         * @param signature
         * @param signatureData
         *
         * @returns {string}
         */
        function getCardDiv(card, signature = false, signatureData) {

            let cd = "<div class=\"response-card\">\n" +
                "       <div class=\"card-text font-weight-bold\">" + card.text + "</div>\n";

            if (signature) {
                cd += "<div class=\"card-signature\">\n" +
                    "               <div class=\"card-signature-container\">\n" +
                    "                   <div class=\"card-logo\"></div>";

                if (signatureData.deck !== undefined) {
                    cd += "<div class=\"card-pack text-muted\">" + signatureData.deck + "</div>";
                } else {
                    cd += "<div class=\"card-pack text-muted\"><div class=\"blank-card\"></div></div>";
                }

                if (signatureData.name !== undefined) {
                    cd += "<div class=\"card-owner text-muted\">" + signatureData.name + "</div>";
                }

                cd += "</div>" +
                    "</div>\n";
            }

            cd += "            </div>\n";

            return cd;


        }

        /**
         * Make an element as selected and remove the previous selected one
         *
         * @param el
         */
        function selectCard(el) {

            if(!isEnabled(sendBtn)){

                enable(sendBtn);
                show(sendBtn);
            }

            if ((window.game.selected !== undefined) && (document.querySelector("[data-id=\"" + window.game.selected.dataset.id + "\"]") !== null))
                document.querySelector("[data-id=\"" + window.game.selected.dataset.id + "\"]").classList.remove("selected");

            if (!el.classList.contains("selectable"))
                return;

            window.game.selected = el;
            el.classList.add("selected");
        }

    </script>

@endsection
