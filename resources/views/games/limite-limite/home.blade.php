@extends('layouts.master')

@section("container", "container-fluid")

@section('content')

    <!-- Page Content -->
    <div class="container-fluid">

        <h3>Bienvenue sur {{ app_name() }}</h3>

        <div class="row text-justify jumbotron mb-0 py-1 c-bg-squared">

        @foreach($games as $game)
            <!-- Partie créée -->
                <div class="col-lg-6 col-12 my-3">
                    <div class="card bg-secondary">

                        <div class="card-header row pb-2 mx-0">
                            <div class="col-8 px-0">{{ $game->name }}</div>
                            <div class="col-4 px-0 text-right">
                                @switch($game->getStatus())
                                    @case($game::STATUS_GAME_OVER)
                                        <i class="fas fa-circle text-danger"></i> {{ trans("app.game_over") }}
                                        @break
                                    @case($game::STATUS_PLAYING)
                                        <i class="fas fa-circle text-success"></i> {{ trans("app.playing") }}
                                        @break
                                    @default
                                        <i class="fas fa-circle text-warning"></i> {{ trans("app.error_happened") }}
                                        @break
                                @endswitch
                            </div>
                        </div>

                        <div class="card-body text-muted">
                            <table class="w-100">
                                <!-- Players, Spectators,  -->
                                <tr class="align-top">
                                    <td class="pb-3">
                                        <strong class="d-md-block">Joueurs</strong>
                                        <p>{{ $game->countPlayers() }}/20</p>
                                    </td>
                                    <td class="pb-3">
                                        <strong class="d-md-block">Spectateurs</strong>
                                        <p>{{ $game->countSpectators() }}/20</p>
                                    </td>
                                    <td class="pb-3 text-right"><strong>{{ $game->getOption("lang") }}</strong></td>
                                </tr>

                                <!-- Points Needed -->
                                <tr class="align-top">
                                    <td colspan="3"><strong>Score max : </strong>{{ $game->getOption("max_points") }}
                                    </td>
                                </tr>

                                <!-- Host Name -->
                                <tr class="align-top">
                                    <td colspan="3"><strong>Hôte : </strong>{{ $game->getHost()->name }}</td>
                                </tr>

                                <!-- Tags -->
                                <tr class="align-top">
                                    <td colspan="3" class="text-justify"><strong>Tags : </strong>{{ $game->getTags()  }}</td>
                                </tr>

                                <!-- Boutons -->
                                <tr class="align-middle">
                                    <td class="font-italic">
                                        <p class="d-none d-md-block m-0">Rejoindre en tant que :</p>
                                    </td>
                                    <td colspan="2" class="text-right">
                                        <span class="d-inline-block" data-toggle="tooltip" data-placement="top"
                                              title="Fonctionnalité à venir">
                                        <a class="btn btn-secondary disabled" type="button" disabled>Observateur</a>
                                    </span>
                                        <a href="{{ route("limite-limite.join", $game->token) }}" class="btn btn-primary">Joueur</a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            @endforeach

        </div>
    </div>

@endsection
