@extends("layouts.master")

@section("title", trans("app.create_game"))

@section("content")

    <div class="content">

        <h3>Création de partie</h3>

        <form method="POST" action="{{ route("limite-limite.store") }}">

            <div class="form-group form-row">
                <div class="col col-md">
                    <label for="name">Nom de la partie :</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>

                @if(!session()->has("username"))
                    <div class="col col-md">
                        <label for="username">Pseudo :</label>
                        <input type="text" class="form-control" name="username" id="username"/>
                    </div>
                @endif

                <div class="col col-md">
                    <label for="lang">Langage de la partie :</label>
                    <select name="lang" id="lang" class="form-control">
                        <optgroup label="Langage de la partie">
                            <option value="fr"> &#127467;&#127479; Français</option>
                            <option value="us"> &#127482;&#127480; Anglais</option>
                        </optgroup>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label for="password">Mot de passe (Optionnel) :</label>
                <input type="text" name="password" id="password" class="form-control">
                <small class="form-text text-muted"><strong>Laissez ce champ vide pour créer une partie
                        publique</strong></small>
            </div>

            <div class="form-group form-row">
                <div class="col col-md mb-3">
                    <label for="max_players">Nombre de joueurs maximum :</label>
                    <input type="text" min="3" max="20" value="3" name="max_players" id="max_players"
                           class="form-control form-control-sm disabled" readonly>
                    <small class="form-text text-muted">Choisissez un nombre entre 3 et 20</small>
                </div>
                <div class="col col-md mb-3">
                    <label for="max_spectators">Nombre d'observateurs maximum :</label>
                    <input type="number" min="0" max="20" value="0" name="max_spectators" id="max_spectators"
                           class="form-control form-control-sm disabled" readonly>
                    <small class="form-text text-muted">Choisissez un nombre entre 0 et 20</small>
                </div>

                <div class="col col-md mb-3">
                    <label for="max_points">Nombre de points pour gagner :</label>
                    <input type="number" min="0" max="20" value="0" name="max_points" id="max_points"
                           class="form-control form-control-sm disabled" readonly>
                    <small class="form-text text-muted">Choisissez un nombre entre 0 et 20</small>
                </div>
            </div>

            <div class="form-group">
                <label for="decks">Deck(s) de jeu :</label>
                <input type="text" name="decks" id="decks" class="form-control">
                <small class="form-text text-muted">Entrez les codes Cardcast désirés, tous séparés par un
                    espace</small>
            </div>

            <!-- Options de membre connecté --
            <div class="form-group d-flex justify-content-center">
                <div class="col-12 col-lg-6 p-0">

                    !-- Is Player Offline --
                    <div class="connected-rules-offline text-muted">
                        Vous pouvez créer un compte et vous connecter afin de configurer et récupérer vos decks favoris.
                    </div>
                    --

                    !-- Is Player Online --
                    <hr>
                    <div id="favorite-decks">

                        <h6>Decks favoris :</h6>

                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="EQSFS" id="EQSFS">
                                    <label class="form-check-label" for="EQSFS">Nom de pack random avec pour code EQSFS</label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="QGDSF" id="QGDSF">
                                    <label class="form-check-label" for="QGDSF">Nom de pack random avec pour code QGDSF</label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="HGSFS" id="HGSFS">
                                    <label class="form-check-label" for="HGSFS">Nom de pack random avec pour code HGSFS</label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="POFSI" id="POFSI">
                                    <label class="form-check-label" for="POFSI">Nom de pack random avec pour code POFSI</label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="DS4DS" id="DS4DS">
                                    <label class="form-check-label" for="DS4DS">Nom de pack random avec pour code DS4DS</label>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="GDSFZ" id="GDSFZ">
                                    <label class="form-check-label" for="GDSFZ">Nom de pack random avec pour code GDSFZ</label>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="6KIF7" id="6KIF7">
                                    <label class="form-check-label" for="6KIF7">Nom de pack random avec pour code 6KIF7</label>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="WCO46" id="WCO46">
                                    <label class="form-check-label" for="WCO46">Nom de pack random avec pour code WCO46</label>
                                </div>
                            </li>
                        </ul>

                    </div>
                    --

                </div>
            </div>
            -->

            @csrf
            <button type="submit" id="submit-form-btn" class="btn btn-block btn-primary">Créer une nouvelle partie</button>

        </form>

    </div>

@endsection
