@extends('layouts.master')

@section('title', 'Hub')

@section('content')

    <h1>Limite-Limite</h1>

    <div class="row justify-content-center">
        <div class="col-8">
            <form action="{{ route('limite-limite.hub') }}" method="POST" style="margin-top: 40px">

                <div class="input-group col-8">
                    <input type="text" name="username" class="form-control" placeholder="Choisir un nom d'utilisateur">
                    <div class="input-group-append">
                        <input type="submit" class="btn btn-primary" value="Choisir"/>
                    </div>
                </div>
                @csrf
            </form>
        </div>
    </div>

    <h3>Liste des parties (in dev)</h3>
    <div class="col-8">
        <ul>
        @foreach($games as $game)

                <li><a href="{{ route("limite-limite.join", $game->token) }}">{{ $game->name }}</a></li>

        @endforeach
        </ul>
    </div>
    <a href="{{ route("limite-limite.create") }}" class="btn btn-primary">Créer</a>

@endsection
