@extends('layouts.master')

@section('title', 'Hub')

@section('content')

    <h1>Limite-Limite</h1>

    <div class="row justify-content-center">
        <div class="col-8">
            <form action="{{ route("limite-limite.store") }}" method="POST">

                <div class="row form-group">
                    <label for="name">Nom de la partie :</label>
                    <input type="text" class="form-control" name="name" id="name" />

                    <label for="password">Mot de passe :</label>
                    <input type="password" class="form-control" name="password" id="password" />
                </div>

                <div class="row form-group">
                    <label for="decks">Decks :</label>
                    <input type="text" class="form-control" name="decks" id="decks" />
                </div>

                @if(!session()->has("username"))
                    <div class="row form-group">
                        <label for="decks">Pseudo :</label>
                        <input type="text" class="form-control" name="username" id="username" />
                    </div>
                @endif

                <div class="form-group">
                    <button class="form-control btn btn-block btn-primary c-btn" id="sendForm" onclick="loadAnimation()">Créer</button>
                </div>
                @csrf
            </form>


        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <div class="row form-group">
            </div>
        </div>
    </div>

@endsection

@section("scripts")

    <script type="text/javascript">

        function loadAnimation() {

            let name = document.getElementById("name");
            let password = document.getElementById("password");
            let decks = document.getElementById("decks");

            name.setAttribute("readonly", "1");
            password.setAttribute("readonly", "1");
            decks.setAttribute("readonly", "1");

            document.getElementById("sendForm").innerHTML = "<div class=\"spinner-border\" role=\"status\"></div>";

        }

    </script>

@endsection
