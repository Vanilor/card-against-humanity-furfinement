@extends('layouts.master')

@section('title', 'Hub')

@section('content')

    <h1>Limite-Limite</h1>

    <div class="row">
        <div id="infos"></div>
    </div>

    <div class="row" id="buttons">
        <button class="btn btn-primary" id="see_last_round-btn" onclick="showLastRound()">Voir le précédent round</button>
        <button class="btn btn-danger" id="leave-btn" onclick="leave()">Quitter</button>
    </div>

    <div class="row" id="currentRound">
        <div class="col-3" id="players" style="background-color: darkgrey; min-height: 50px;"></div>
        <div class="col-3" id="all_cards" style="background-color: dodgerblue; min-height: 50px;">
             <div id="played-cards-nb"></div>
             <div id="my-cards"></div>
        </div>
        <div class="col-3" id="calls_messages">
        </div>
        <div class="col-3" id="responses_messages">
        </div>
    </div>

    <div class="row d-none" id="lastRound">

    </div>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal">Large modal</button>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content"
                 style="height: 100px; background-image: url('{{ asset("images/confetti.gif") }}'); background-size: cover; ">
                <span class="align-middle text-center font-weight-bold" style="margin-top: 25px">Un joueur random a gagné</span>
            </div>
        </div>
    </div>

@endsection

@section("scripts")

    <script type="text/javascript">

        /**
         * @TODO Add player_api_token and JSON datatype in Ajax setup (check all API returns before, do this during code refactoring)
         */

        window.selected = undefined;
        window.checkGame = setInterval(checkRound, 1500);
        window.isGameOver = 0;
        const player_api_token = "{{ session()->get('player_api_token') }}";

        let callCards = $("#calls_messages");
        let responseCards = $("#responses_messages");
        let info = $("#infos");
        let playersStatus = $("#players");
        let buttons = $("#buttons");
        let lastRound = $("#lastRound");
        let currentRound = $("#currentRound");
        let seeLastRoundBtn = $("#see_last_round-btn");
        let playedCardsNb = $("#played-cards-nb");
        let myCards = $("#my-cards");
        let sendBtn;
        let retrieveBtn;

        $(document).ready(() => {

            buttons.append(getConfirmButton());
            buttons.append(getRetrieveButton());

            sendBtn = document.getElementById("btn-send");
            retrieveBtn = document.getElementById("btn-retrieve");

        });

        function sendCard() {

            if (window.selected === undefined) {
                notify("warning", 'fas fa-exclamation-circle', "{{ trans("app.please_choose_card") }}");
                return;
            }

            disable(sendBtn);
            disable(retrieveBtn);

            $.ajax({

                method: "PUT",
                url: "{{ route("api.limite-limite.play-card", $token) }}",
                data: {
                    player_api_token: player_api_token,
                    card_id: window.selected.dataset.id
                },
                datatype: "json",
                success: function (data) {

                    window.selected = undefined;
                    enable(retrieveBtn);

                    //You're czard error message
                    if (data.status === "unauthorized") {
                        notify("warning", "fas fa-exclamation-circle", data.message.text);
                        //responseCards.append(getCardDiv(data.message.played_card));
                    }

                    else if(data.status === "czard_choosen"){

                        playersStatus.html(generatePlayersStatus(data));
                        myCards.html(generatePlayedResponses(data));
                        $("[data-ident="+data.message.winner+"]").addClass("text-success");
                        stopChecker(3000);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message)

                }

            });

            window.selected.remove();

        }

        function retrieveLastPlayedCard() {

            window.selected = undefined;
            myCards.html("Vous avez joué : ");

            $.ajax({

                method: "GET",
                url: "{{ route("api.limite-limite.retrieve-last", $token) }}",
                data: {
                    player_api_token: player_api_token,
                },
                dataType: "JSON",
                success: function (data) {

                    disable(retrieveBtn);

                    if (data.status === "unauthorized") {
                        notify("warning", "fas fa-exclamation-circle", data.message);
                    }

                    else {

                        for (let el of data.message)
                            responseCards.append(getCardDiv(el.card));

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);

                }

            });

        }

        function checkRound() {

            $.ajax({

                method: "GET",
                url: "{{ route("api.limite-limite.check-round", $token) }}",
                data: {
                    player_api_token: player_api_token
                },
                datatype: "json",
                success: function (data, textStatus, jqHXR) {

                    if(window.isGameOver)
                        info.html("");

                    if (retrieveBtn.classList.contains("d-none"))
                        retrieveBtn.classList.remove("d-none");

                    if (sendBtn.classList.contains("d-none"))
                        sendBtn.classList.remove("d-none");



                    if (data.status === "not_found") {

                        window.location.href = "{{ route("limite-limite.hub") }}";

                    }

                    //They are missing players. Everyone's waiting
                    else if (data.status === "waiting") {

                        info.html("");
                        myCards.html("");
                        playedCardsNb.html(data.message.missing);
                        playersStatus.html("");
                        callCards.html("");
                        responseCards.html("");

                    }

                    //The player is czard and waiting for regular players to play
                    else if (data.status === "czard_waiting") {

                        myCards.html("{{ trans("api.limite_limite.missing_players_czard", ["missing" => 0]) }}".replace("0", data.message.missing));
                        responseCards.html("{!! trans("app.limite-limite.youre_czard") !!}");
                        retrieveBtn.classList.add("d-none");

                    }

                        //No call card send by API in case of round end and player is czard
                    //The player is currently czard and is choosing a response card
                    else if (data.status === "czard_choosing") {

                        responseCards.html("{!! trans("app.limite-limite.youre_czard") !!}");
                        retrieveBtn.classList.add("d-none");
                        playedCardsNb.html("Nombre de cartes jouées : " + data.message.total);

                        let list = "<ul>";
                        let pDiv = "";

                        let str = "";
                        for (const player of data.message.cards) {

                            str = "CARDS <ul>";
                            for (const card of player.cards)
                                str += "<li>Deck : "+card.deck+"<br /> Text :"+ card.text + "</li>";
                            str += "</ul>";


                            pDiv = "<div class=\"c-card ";

                            //Add the text success class to this element if it was previously selected in order to
                            //prevent refresh disappearing
                            //@TODO Find a better way to do that
                            if (window.selected !== undefined)
                                if (window.selected.dataset.id == player.response_id)
                                    pDiv += "text-success";

                            pDiv += "\" onclick='selectCard(this)' data-id=\"" + player.response_id + "\">" + str + "</div>";

                            list += "<li>" + pDiv + "</li>";
                        }

                        myCards.html(list);
                    }

                    else if (data.status === "game_over") {

                        responseCards.html("");
                        callCards.html("");
                        info.html("");

                        //Vanish buttons
                        disable(sendBtn);
                        sendBtn.classList.add("d-none");
                        disable(retrieveBtn);
                        retrieveBtn.classList.add("d-none");

                        $(".text-success").removeClass("text-success");
                        $("[data-ident='"+data.message.winner+"'").addClass("text-danger");

                        if(!(window.isGameOver))
                            window.isGameOver = 1;

                        if ((data.message.restart_token !== undefined) && (!$("#btn-restart").length)) {
                            buttons.append("<button class='btn btn-primary' id='btn-restart' onclick='restartGame(\"" + data.message.restart_token + "\")'>Relancer une partie</button>");
                        }

                    }

                    //The player is a regular one and is on-game
                    else {

                        //Disable the Retrieve button when the call card change
                        if (data.message.call_card !== callCards.text())
                            disable(retrieveBtn);

                        if ((data.status === "round_over") || (data.status === "czard_choosen")) {

                            myCards.html(generatePlayedResponses(data));

                        }

                        else {

                            let playedData = data.message.response_cards_played.by_me;
                            let playedCards = "<ul>";

                            if (playedData != false) {

                                for (let el of playedData)
                                    playedCards += "<li>" + el.card.text + "</li>";

                                enable(retrieveBtn);
                            }
                            playedCards += "</ul>";

                            playedCardsNb.html("Nombre de cartes jouées : " + data.message.response_cards_played.total);
                            myCards.html("Vous avez joué : " + playedCards);

                        }
                        //Purge current responses cards
                        responseCards.html("");

                        let div;

                        //Display responses cards
                        for (const card of data.message.my_cards) {

                            //Add the text success class to this element if it was previously selected in order to
                            //prevent refresh disappearing
                            //@TODO Find a better way to do that)
                            responseCards.append(getCardDiv(card, (
                                        (window.selected !== undefined) && (window.selected.dataset.id == card.id)
                                    )
                                ));
                        }

                    }

                    //Generates last round div
                    let lastRoundHTML = "";

                    let lastRoundPlayerStatus = "<ul style='background-color: darkgrey'>";
                    for (const player of data.message.last_round.players) {

                        let li = "";
                        if(player.is_winner)
                            li = "<li class='text-success' data-ident='"+player.ident+"'>";
                        else
                            li = "<li>";

                        lastRoundPlayerStatus += li+"Name : " + player.name + " <br /> Score : " + player.score + "<br />";

                        let cardsList = "<ul>";
                        for (const card of player.cards)
                            cardsList += ("<li>" + card + "</li>");
                        cardsList += "</ul>";

                        lastRoundPlayerStatus += cardsList + "</li>";

                    }

                    lastRoundPlayerStatus += "</ul>";
                    lastRoundHTML += lastRoundPlayerStatus;
                    lastRoundHTML += "<div>" + data.message.last_round.call_card + "</div><br /><br />";

                    lastRound.html(lastRoundHTML);

                    //Displays call card
                    if(data.status !== "game_over")
                        callCards.html(data.message.call_card);

                    playersStatus.html(generatePlayersStatus(data));

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);

                }

            });

        }

        function restartGame(token) {

            $.ajax({

                method: "PUT",
                url: "{{ route("api.limite-limite.restart", $token) }}",
                dataType: "json",
                data: {
                    player_api_token: player_api_token,
                    restart_token: token,
                },
                success: function (data) {
                    $("#btn-restart").remove();
                    info.html("");
                },
                error: function (jqXHR, textStatus, errorThrown) {

                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);

                }
            });

        }

        function leave(){

            $.ajax({
                method: "PUT",
                url: "{{ route("api.limite-limite.leave", $token) }}",
                dataType: "json",
                data: {
                    player_api_token: player_api_token,
                },
                success:  function () {

                    window.location.href = "{{ route("limite-limite.hub") }}";

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notify("warning", "fas fa-times", jqXHR.responseJSON.message);
                }
            })

        }

        /*
         * ==============================
         * Utils functions
         * ==============================
         */

        function generatePlayedResponses(data){

            let str = "";
            let list = "<ul>";
            let displayNames = (data.status === "czard_choosen");

            for (const pl of data.message.response_cards_played) {

                console.log("winner/ident : "+data.message.winner+"/"+pl.ident);

                if(displayNames && (data.message.winner == pl.ident))
                    str = "<ul class=\"text-success\">";
                else
                    str = "<ul>";

                if(displayNames)
                    str += "<li>NAME : "+pl.name+"</li>";


                str += "<li> CARDS : <ul>";

                for (const card of pl.cards)
                    str += "<li> Deck : " + card.deck + "<br /> Text :" + card.text + "</li>";

                str += "</ul></li></ul>";

                list += "<li>" + str + "</li>";
            }

            return list;

        }

        function generatePlayersStatus(data){

            //Display player sidebar
            let currentPlayerStatus = "<ul>";
            let li;
            for (const player of data.message.players) {

                if(((data.status === "czard_choosen") || (data.status === "game_over")) && (player.ident === data.message.winner)){

                    li = "<li class='text-success' data-ident='"+player.ident+"'>";

                    if(data.status === "czard_choosen")
                        player.score++; //Display updated score instead of old one

                }
                else
                    li = "<li data-ident='"+player.ident+"'>";

                currentPlayerStatus += li+"Name : " + player.name + " <br /> Score : " + player.score + "</li>";
            }

            currentPlayerStatus += "</ul>";
            return currentPlayerStatus;

        }

        /**
         * Show last round
         */
        function showLastRound() {

            if (lastRound.hasClass("d-none")) {
                seeLastRoundBtn.html("{{ trans("app.see_current_round") }}");
                currentRound.addClass("d-none");
                lastRound.removeClass("d-none");
            } else {
                seeLastRoundBtn.html("{{ trans("app.see_last_round") }}");
                currentRound.removeClass("d-none");
                lastRound.addClass("d-none");
            }


        }

        /**
         * Enables an element by removing disabled properties and style
         *
         * @param el
         */
        function enable(el) {

            el.removeAttribute("disabled");
            el.classList.remove("disabled");

        }

        /**
         * Disable an element by adding disabled properties and style
         *
         * @param el
         */
        function disable(el) {
            el.setAttribute("disabled", "disabled");
            el.classList.add("disabled");
        }

        /**
         *
         * Generates a card div by getting the API content descriptor
         *
         * @param card
         * @param isSelected
         *
         * @returns {string}
         */
        function getCardDiv(card, isSelected = false) {

            if (isSelected)
                return "<div class=\"c-card text-success\" data-id=\"" + card.id + "\" onclick=\"selectCard(this)\">" + card.text + "</div>";
            else
                return "<div class=\"c-card\" data-id=\"" + card.id + "\" onclick=\"selectCard(this)\">" + card.text + "</div>";

        }

        /**
         * Make an element as selected and remove the previous selected one
         *
         * @param el
         */
        function selectCard(el) {

            if (sendBtn.hasAttribute("disabled") || sendBtn.classList.contains("disabled"))
                enable(sendBtn);

            if ((window.selected !== undefined) && (document.querySelector("[data-id=\"" + window.selected.dataset.id + "\"]") !== null))
                document.querySelector("[data-id=\"" + window.selected.dataset.id + "\"]").classList.remove("text-success");

            window.selected = el;
            el.classList.add("text-success");
        }

        /**
         * Returns the Retrieve button element
         *
         * @returns {string}
         */
        function getRetrieveButton() {
            return '<button class="btn btn-secondary disabled" id="btn-retrieve" onclick="retrieveLastPlayedCard()" disabled>Récupérer ma carte</button>';
        }

        /**
         * Returns the Confirmation button element
         *
         * @returns {string}
         */
        function getConfirmButton() {
            return '<button class="btn btn-secondary disabled" id="btn-send" onclick="sendCard()" disabled>Confirmer</button>';
        }

        /**
         * Stops the round checker for the given timer param, in seconds
         * @param timer
         */
        function stopChecker(timer){

            clearInterval(window.checkGame);

            setTimeout(() => {
                window.checkGame = setInterval(checkRound, 1500);
            }, timer);

        }

    </script>

@endsection
