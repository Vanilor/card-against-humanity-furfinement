@extends('layouts.master')

@section('title', 'Hub')

@section('content')

    <h1>Limite-Limite</h1>
    <h4>{{ $game->name }}</h4>

    <div class="row justify-content-center">
        <div class="col-8">
            <form action="{{ route('limite-limite.join-request', $token) }}" method="POST" style="margin-top: 40px">

                <input type="text" name="username" id="username"
                       class="form-control" placeholder="Choisir un nom d'utilisateur"
                        value="{{ session()->has("username") ? session()->get("username") : old("username") }}">

                @if($game->hasPassword())
                    <input type="password" name="password" class="form-control" placeholder="Entrez le mot de passe">
                @endif
                @csrf

                <input type="submit" class="btn btn-primary" value="Rejoindre"/>
            </form>
        </div>
    </div>

@endsection
