<script type="text/javascript">
    @foreach (session('flash_notification', collect())->toArray() as $message)

        $.notify({
            icon: 'fa fa-times',
            message: "{{ $message['message'] }}",
        }, {

            type: "{{ $message['level'] }}",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            delay: 2500,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
        });

    @endforeach
</script>

@php(session()->forget('flash_notification'))
