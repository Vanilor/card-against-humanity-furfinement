<?php

namespace App\Http\Controllers;

use App\Http\Objects\Cards;
use App\Models\LimiteLimiteGame;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public Collection $cardSet;

    public function __construct()
    {
        $set = new Cards();
        $this->cardSet = $set->getNewSet();
    }

    public function home(){

        return view('home');

    }

    public function setUsername(Request $request){

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ]);

        if($validator->fails()){

            foreach ($validator->errors()->all() as $error)
                flash($error, 'danger');

            return Redirect::back();

        }

        session()->put('username', $request->get('username'));

        return Redirect::back();

    }

    public function earlyAccess(){

        return view("early-access");

    }

    public function checkEarlyAccessToken(Request $request){

        if(!$request->has("token") || empty($request->get("token"))){

            flash("Entrez un token d'accès pour pouvoir vous enregistrer")->error();
            return Redirect::route("early-access");

        }

        if(array_search($request->get('token'), config("early_access_token")) !== false){

            flash("Bienvenue dans cette version pré-release ! :D")->success();
            session()->put("early_access_token", $request->get("token"));
            return Redirect::route("limite-limite.hub");

        }

        flash("Le token d'accès entré n'est pas valide :(")->error();
        return Redirect::route("early-access");

    }

}
