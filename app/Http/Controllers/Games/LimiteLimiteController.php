<?php

namespace App\Http\Controllers\Games;

use App\Http\Controllers\Api\Requester\DeckManagerRequesterAPI;
use App\Http\Controllers\Controller;
use App\Models\LimiteLimiteGame;
use App\Objects\Player;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class LimiteLimiteController extends Controller
{

    public function home()
    {

        return View::make('games.limite-limite.home')->with(['games' => LimiteLimiteGame::all()]);

    }

    public function create()
    {

        return View::make('games.limite-limite.create');

    }

    public function store(Request $request)
    {

        $rules = [
            "name" => "required|string",
        ];

        if(!session()->has("username"))
            $rules["username"] = "required|string";

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error)
                flash($error)->error();
        }

        $decks = [];
        if ($request->get("decks") !== null) {

            $decks = explode(" ", $request->get("decks"));
            $decks = DeckManagerRequesterAPI::getMultipleDecks($decks);

            $decks["responses"] = [];
            $decks["calls"] = [];


            foreach ($decks as $key => $deck) {

                if ($key !== "responses" && $key !== "calls") {

                    //Reduce CardCastGame API calls from two to one
                    if (($deck->response_count > 0) && ($deck->call_count > 0)) {

                        $cards = DeckManagerRequesterAPI::getAllDeckCards($deck->id);
                        $decks["responses"][$deck->id] = $cards->responses;
                        $decks["calls"][$deck->id] = $cards->calls;



                    }
                    else if ($deck->response_count > 0)
                        $decks["responses"][$deck->id] = DeckManagerRequesterAPI::getResponsesCardsFromDeck($deck->id);


                    else if ($deck->call_count > 0)
                        $decks["calls"][$deck->id] = DeckManagerRequesterAPI::getCallsCardsFromDeck($deck->id);

                    unset($decks[$key]);

                }
            }

        }

        if(!session()->has("username")){

            session()->put("username", $request->get("username"));
            $hostName = $request->get("username");

        }
        else
            $hostName = session()->get("username");

        $host = new Player($hostName, 0, false, 0, [], $decks["responses"], true);
        $host->shuffleDeck();
        $host->setIdentifier(0);

        if (session()->has("player_api_token"))
            session()->forget("player_api_token");

        session()->put("player_api_token", $host->getApiToken());

        $players = array();
        $players[$host->getApiToken()] = $host;

        $game = new LimiteLimiteGame();

        $game->token = utf8_encode(Str::random(16));
        $game->restart_token = utf8_encode(Str::random(16));
        $game->name = $request->get('name');
        $game->password = Hash::make($request->get('password'));
        $game->host = $host->getApiToken();
        $game->ident_index = 0;
        $game->options = $game->getDefaultOptions();
        $game->current_round = $game->generateCurrentRound($decks["calls"]);
        $game->last_round = $game->generateLastRound();
        $game->players = $players;
        $game->czard_index = 0;
        $game->calls_decks = $decks["calls"];
        $game->responses_decks = $decks["responses"];
        $game->active = 0;
        $game->created_at = Carbon::now();
        $game->updated_at = Carbon::now();

        $game->save();

        flash("Partie créée avec succès")->success();
        return Redirect::route("limite-limite.show", $game->token);

    }

    public function show(Request $request, string $token)
    {

        $game = LimiteLimiteGame::where('token', '=', $token)->first();
        if(!$game) {
            flash("Cette partie n'a pas pu être trouvée")->error();
            return Redirect::back();
        }

        $api_token = session()->get("player_api_token");

        if ($api_token == null || $game->getPlayer($api_token) === NULL) {
            flash("Vous ne pouvez pas accéder à cette partie")->warning();
            return Redirect::route('limite-limite.join', $token);
        }

        $data = [];
        $data["token"] = $token;
        $data["hasPlayed"] = !$game->isPlayerHandEmpty($api_token);
        $data["player_hand"] = [];

        return View::make("games.limite-limite.show")->with($data);

    }

    public function join(Request $request, $token = null){

        if($token === null){

            flash(trans("app.no_choosen_game"))->warning();
            return Redirect::route("limite-limite.hub");

        }

        $game = LimiteLimiteGame::where('token', '=', $token)->first();

        if(!$game) {
            flash("Cette partie n'a pas pu être trouvée")->error();
            return Redirect::back();
        }

        if(session()->has("player_api_token") && !$game->hasPassword()){

            if($game->getPlayer(session("player_api_token")) !== NULL){

                flash(trans("app.welcome_back"))->info();
                return Redirect::route("limite-limite.show", $token);

            }

            else {

                return $this->joinRequest($request, $token);

            }

        }

        $data = [];
        $data["game"] = $game;
        $data["token"] = $token;

        return View::make("games.limite-limite.join")->with($data);

    }

    public function joinRequest(Request $request, $token = null){

        $game = LimiteLimiteGame::where('token', '=', $token)->first();

        if(!$game) {
            flash("Cette partie n'a pas pu être trouvée")->error();
            return Redirect::back();
        }

        if(session()->has("player_api_token")){

            if($game->getPlayer(session("player_api_token")) !== NULL){

                flash(trans("app.welcome_back"))->info();
                return Redirect::route("limite-limite.show", $token);

            }

            else {

                return $this->enterGame($request, $game);

            }

        }

        $rules = [];
        $rules["username"] = "required";

        if($game->hasPassword())
            $rules["password"] = "required";


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error)
                flash($error)->error();

            return Redirect::back()->withInput(["username" => $request->get('username')]);
        }

        if(!Hash::check($request->get('password'), $game->password)){

            flash(trans("app.invalid_password"))->error();
            return Redirect::back()->withInput(["username" => $request->get('username')]);

        }

        return $this->enterGame($request, $game);

    }

    private function enterGame(Request $request, LimiteLimiteGame $game){

        $username = session()->has("username") ? session()->get('username') : $request->get('username');

        if($game->doesUsernameExist($username)){

            session()->forget("username");
            session()->put("username", $username);
            flash(trans("app.username_already_taken"))->error();
            return Redirect::back();

        }

        $player = new Player($username, 0, false, 0, [], $game->responses_decks, true);
        $player->shuffleDeck();

        if (session()->has("player_api_token"))
            session()->forget("player_api_token");

        session()->put("player_api_token", $player->getApiToken());
        session()->put("username", $player->name);

        $game->addPlayer($player);

        return Redirect::route("limite-limite.show", $game->token);
    }

}
