<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Objects\Cards;
use App\Models\GameToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;

class CRUDAction extends Controller
{

    protected $model;
    protected $token;

    public function __construct(Request $request)
    {

        if (Route::currentRouteName() !== "api.crud.store") {

            $this->token = $request->get('token');
            $this->model = DB::table('game_tokens')
                ->where('token', '=', $this->token)
                ->select('model')
                ->limit(1)
                ->get();

        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'string',
        ]);

        if ($validator->fails()) {

            return response()->json([
                "status" => 501,
                "errors" => $validator->errors()->all(),
            ]);

        }

        $token = new GameToken();

        $token->token = Str::random();
        $token->model = $request->get("model"); //Need an implementation

        $token->save();

        $name = $request->get('name');
        if (empty($request->get('name')))
            $name = Str::random(16);

        $cardSetID = DB::table('card_sets')->insertGetId([
            'collection' => json_encode(Cards::generateSet()),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $token = Uuid::generate(4)->string;

        $game = new $this->model();

        $game->token = $token;
        $game->name = $name;
        $game->players_nb = 1;
        $game->card_set_id = $cardSetID;
        $game->active = 1;

        $game->created_at = Carbon::now();
        $game->updated_at = Carbon::now();

        $game->save();

        return response()->json([
            "status" => 200,
        ]);

    }

    public function setName(Request $request)
    {

        $game = $this->model::findOrFail($this->getIDFromToken($this->token));
        $game->name = $request->get('name');
        $game->save();

        return;

    }

    public function incrementPlayersNb()
    {

        $this->model::findOrFail($this->getIDFromToken($this->token))->increment('players_nb');
        return;

    }

    public function decrementPlayersNb()
    {

        $this->model::findOrFail($this->getIDFromToken($this->token))->decrement('players_nb');
        return;

    }

    public function setActive(Request $request)
    {

        $game = $this->model::findOrFail($this->getIDFromToken($this->token));
        $game->active = $request->get('active');
        $game->save();

        return;
    }

    private function getIDFromToken(string $token)
    {

        $game = $this->model::where([
            ['token', '=', $token]
        ])->select('id')->limit(1)->get();

        if ($game->isEmpty())
            return NULL;

        return $game->id;

    }

}
