<?php


namespace App\Http\Controllers\Api;

use App\Models\LimiteLimiteGame;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

/**
 * Class LimiteLimiteAPI
 * @package App\Http\Controllers\Api
 */
class LimiteLimiteAPI extends Api implements IApi
{

    private string $game_id;
    private string $player_api_token;
    private LimiteLimiteGame $game;

    /**
     * @TODO Create an API middleware for allowRequest() method
     */

    /**
     * LimiteLimiteAPI constructor.
     *
     * @param Request $request
     *
     */
    public function __construct(Request $request)
    {
        $this->game_id = $request->route('game_id');
        $this->player_api_token = $request->get('player_api_token') ?? "";
    }

    /**
     *
     * @return JsonResponse
     */
    public function getResponseCards()
    {

        if (!$this->allowRequest())
            return $this->makeResponse("not_found", 404, trans("api.game_not_found", ["token" => $this->game_id]));


        if ($this->game->isPlayerCzard($this->player_api_token))
            return $this->makeResponse("success", 200, []);


        if (empty($this->game->responses_decks))
            return $this->makeResponse("success", 200, []);


        // Send player current hand
        $selected = $this->game->getPlayerHand($this->player_api_token);

        //Get the amount of cards that's missing
        $missing_cards_nb = $this->game->getPlayerMissingCardsNb($this->player_api_token);

        //If the player has not enough cards, giving them to it
        if (!$this->game->hasEnoughCardsInGame($this->player_api_token)) {

            for ($i = 0; $i < $missing_cards_nb; $i++) {

                $player_card_index = $this->game->getPlayerCurrentResponseIndex($this->player_api_token);

                $this->game->incrementPlayerIndex($this->player_api_token);

                if ($this->game->resetPlayerIndexIfNecessary($this->player_api_token) === true)
                    $this->game->shufflePlayerDeck($this->player_api_token);

                $this->game->addCardToPlayerHand($this->player_api_token, $player_card_index);


                array_push($selected, [
                    "id" => $player_card_index,
                    "text" => $this->game->getCardTextFromDescriptor(
                        $this->game->getCardDescriptorFromIndex($this->player_api_token, $player_card_index)
                    ),
                ]);

                //Refresh Game status
                $this->getGame();

            }

        }

        return $this->makeResponse("success", 200, $selected);

    }


    /**
     * Handle the played card action
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function playCard(Request $request)
    {

        if (!$this->allowRequest())
            return $this->makeResponse("not_found", 404, trans("api.game_not_found", ["token" => $this->game_id]));

        //Validate the request
        $validator = Validator::make($request->all(), [
            "card_id" => "required|string",
        ]);

        if ($validator->fails())
            return $this->makeResponse("error", 400, trans("api.limite_limite.invalid_parameters"));


        //Set last activity in order to avoid timeout
        //@TODO Handle czard timeout
        if (!$this->game->isPlayerCzard($this->player_api_token))
            $this->game->setLastActivity($this->player_api_token);

        //If the round is ended
        if ($this->game->isRoundOver()) {

            //The player is czard and the round is over, then he chosen the card
            if ($this->game->isPlayerCzard($this->player_api_token)) {

                if (!$request->has("card_id"))
                    return $this->makeResponse("Bad request", 400, trans("api.limite_limite.invalid_parameters"));

                $this->game->setCzardChoosenTime();
                $this->game->setRoundWinner($request->get("card_id"));

                return $this->makeResponse("czard_choosen", 200, [

                    "winner"                => $this->game->getWinner(),
                    "players"               => $this->game->getPlayersStatus($this->player_api_token),
                    "response_cards_played" => $this->game->getAllPlayedCards(true),

                ]);

            }
            //The player is a regular one and the round is over, then it cannot retrieve it's card
            else {

                $this->makeResponse("unauthorized", 403, trans("api.limite_limite.round_over"));

            }

        }

        //If the round is not over
        //Check if player is currently czard or already played. Move him away if so and returns his played card
        if (
            ($czard = $this->game->isPlayerCzard($this->player_api_token)) ||
            (
                $this->game->hasPlayed($this->player_api_token) &&
                ($this->game->countPlayedCardsByPlayer($this->player_api_token) >= $this->game->getBlanksInCall())
            )
        ) {

            return $this->makeResponse("unauthorized", 200, [

                "text" => $czard ? trans("api.limite_limite.cant_play") : trans("api.limite_limite.already_played"),
                "played_card" => [
                    "id" => $request->get("card_id"),
                    "text" => $this->game->getCardTextFromPlayerHandById($this->player_api_token, $request->get("card_id"))
                ],

            ]);

        }

        //If false, the player doesn't own the played card, otherwise it has been removed
        if ($this->game->removeCardFromPlayerHand($this->player_api_token, (int)$request->get("card_id")) === false)
            return $this->makeResponse("error", 401, trans("api.limite_limite.dont_own_card"));


        $this->game->addCardToCurrentRound($this->player_api_token, $request->get('card_id'));

        //The player owns the card and the card was removed successfully
        return $this->makeResponse("success", 200, $this::DEFAULT_STATUS);

    }


    /**
     * Retrieve the last played card by a regular player
     * Remove the card from current round, and giving it back into his hand
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function retrieveLast()
    {

        if (!$this->allowRequest())
            return $this->makeResponse("not_found", 404, trans("api.game_not_found", ["token" => $this->game_id]));

        if ($this->game->isRoundOver())
            return $this->makeResponse("unauthorized", 403, trans("api.limite_limite.round_over"));

        if ($this->game->hasEverPlayedDuringRound($this->player_api_token) === false)
            return $this->makeResponse("unauthorized", 200, trans("api.limite_limite.didnt_play"));


        $cards = $this->game->getPlayedCardByPlayer($this->player_api_token);

        if (empty($cards))
            return $this->makeResponse("unauthorized", 200, trans("api.limite_limite.already_retrieved"));

        foreach ($cards as $card)
            $this->game->addCardToPlayerHand($this->player_api_token, $card["id_in_hand"]);

        $this->game->removeCardsFromCurrentRound($this->player_api_token);

        return $this->makeResponse("success", 200, $cards);

    }

    /**
     * Check the current round status and get data
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function checkRound()
    {

        if (!$this->allowRequest())
            return $this->makeResponse("not_found", 404, trans("api.game_not_found", ["token" => $this->game_id]));

        //Indicates that the player is still alive
        $this->game->setAlive($this->player_api_token);
        $this->game->purgeDeadPlayers();

        $callCardText = $this->game->generateCallCard();
        $playersStatus = $this->game->getPlayersStatus($this->player_api_token);

        //Make this a priority and throw it's response first if necessary
        if (($winner = $this->game->isGameOver()) !== false) {

            $message = [
                "last_round" => $this->game->getLastRound(),
                "call_card" => $callCardText,
                "players" => $playersStatus,
                "winner" => $winner,
            ];

            if ($this->game->isHost($this->player_api_token))
                $message["restart_token"] = $this->game->getRestartToken();

            return $this->makeResponse("game_over", 200, $message);

        }

        if ((env("APP_DEBUG") == false) && (($missing = $this->game->areMissingPlayers()) > 0)) {

            return $this->makeResponse("waiting", 200, [
                "players" => $playersStatus,
                "last_round" => $this->game->getLastRound(),
                "missing" => trans("api.limite_limite.missing_players", ["missing" => $missing])
            ]);

        }


        if (!$this->game->hasCzard())
            $this->game->electNewCzard("");


        if ($this->game->isPlayerCzard($this->player_api_token)) {

            if ($this->game->isRoundOver()) {

                $winner_api_token = $this->game->getWinner(true);

                if($winner_api_token === false){

                    //The round is ended, let the czard choose
                    return $this->makeResponse("czard_choosing", 200, [

                        "last_round" => $this->game->getLastRound(),
                        "call_card" => $callCardText,
                        "players" => $playersStatus,
                        "total" => $this->game->countPlayedCards(),
                        "response_cards_played" => $this->game->getAllPlayedCards(false)

                    ]);

                }

                else {

                    if(Carbon::make($this->game->getCzardChoosenTime())->diffInSeconds(Carbon::now()) >= LimiteLimiteGame::SECONDS_BEFORE_NEXT_ROUND){

                        $this->game->saveCurrentRound();

                        $this->game->incrementPlayerScore($winner_api_token);
                        $this->game->incrementCallIndex();
                        $this->game->startNewRound();
                        $this->game->electNewCzard($this->player_api_token);

                        return $this->makeResponse("czard_choosen", 200, $this::DEFAULT_STATUS);

                    }
                    else {

                        return $this->makeResponse("czard_choosen", 200, [

                            "last_round" => $this->game->getLastRound(),
                            "call_card" => $callCardText,
                            "players" => $playersStatus,
                            "my_cards" => $this->getResponseCards()->getOriginalContent()["message"],
                            "response_cards_played" => $this->game->getAllPlayedCards(true),
                            "winner" => $this->game->getWinner()

                        ]);


                    }

                }

            } //If not, retrieve missing players
            else {

                return $this->makeResponse("czard_waiting", 200, [

                    "last_round" => $this->game->getLastRound(),
                    "call_card" => $callCardText,
                    "players" => $playersStatus,
                    "missing" => $this->game->getHadntPlayPlayers(),

                ]);

            }

        } //The player is a regular one, not a czard
        else {

            if ($this->game->isRoundOver()) {

                $winner = $this->game->getWinner();

                return $this->makeResponse(($winner === false) ? "round_over" : "czard_choosen", 200,
                    [
                        "last_round" => $this->game->getLastRound(),
                        "call_card" => $callCardText,
                        "players" => $playersStatus,
                        "my_cards" => $this->getResponseCards()->getOriginalContent()["message"],
                        "response_cards_played" => ($winner === false) ? $this->game->getAllPlayedCards(false) : $this->game->getAllPlayedCards(true),
                        "winner" => $winner
                    ]
                );

            } else {

                return $this->makeResponse("success", 200,
                    [
                        "last_round" => $this->game->getLastRound(),
                        "call_card" => $callCardText,
                        "players" => $playersStatus,
                        "my_cards" => $this->getResponseCards()->getOriginalContent()["message"],
                        "response_cards_played" => [

                            "total" => $this->game->countPlayedCards(),
                            "by_me" => $this->game->getPlayedCardByPlayer($this->player_api_token),

                        ]
                    ]
                );

            }

        }

    }

    public function getStatus(){

        return $this->makeResponse($this->game->getGameStatus()->toString(), 200, []);

    }


    /**
     *
     * Restart the game and launch another !
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function restart(Request $request)
    {

        if (!$this->allowRequest())
            return $this->makeResponse("not_found", 404, trans("api.game_not_found", ["token" => $this->game_id]));

        if (!$request->has("restart_token") || ($request->get("restart_token") !== $this->game->getRestartToken()))
            return $this->makeResponse("Bad request", 400, trans("api.limite_limite.invalid_parameters"));

        if (!$this->game->isHost($this->player_api_token))
            return $this->makeResponse("unauthorized", 401, trans("api.limite_limite.insufficient_permissions"));

        $this->game->restartGame();
        $this->game->regenerateRestartToken();

        return $this->makeResponse("success", 200, $this::DEFAULT_STATUS);

    }

    public function leave(){

        if(!$this->allowRequest())
            return $this->makeResponse("not_found", 404, trans("api.game_not_found", ["token" => $this->game_id]));

        if($this->game->isPlayerCzard($this->player_api_token))
            return $this->makeResponse("unauthorized", 403, trans("api.limite_limite.cant_quit_czard"));

        if($this->game->isHost($this->player_api_token))
            $this->game->electRandomHost($this->player_api_token);

        $this->game->removePlayer($this->player_api_token);

        flash(trans("app.limite-limite.byebye"))->success();
        return $this->makeResponse("success", 200, $this::DEFAULT_STATUS);

    }

    /**
     * Check if game exists and set Object variable
     * @return bool
     *
     */
    private function getGame()
    {

        $this->game = LimiteLimiteGame::where("token", "=", $this->game_id)->first();

        if (!$this->game)
            return false;

        return true;

    }

    /**
     * Use this function when using a route that interact with DB instead orf checkGame() and checkPlayer() directly
     *
     * @return bool
     *
     */
    public function allowRequest()
    {

        if ($this->getGame() && isset($this->game->players[$this->player_api_token]))
            return true;

        return false;

    }

}
