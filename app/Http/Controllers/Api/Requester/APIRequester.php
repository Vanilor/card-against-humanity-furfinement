<?php

namespace App\Http\Controllers\Api\Requester;

class APIRequester {

    public static function sendAPIRequest(string $url)
    {

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL             => $url,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_TIMEOUT         => 10,
            CURLOPT_CONNECTTIMEOUT  => 10,
            //@TODO Remove below entries in production
            CURLOPT_SSL_VERIFYHOST  => 0,
            CURLOPT_SSL_VERIFYPEER  => 0,
        ]);

        $response = curl_exec($ch);

        if ($error = curl_errno($ch)) {

            abort_custom(501, trans("api.crash", ["data" => $error]));

        }

        curl_close($ch);

        return json_decode($response);

    }

}
