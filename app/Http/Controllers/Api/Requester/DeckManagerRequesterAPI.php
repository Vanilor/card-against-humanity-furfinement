<?php


namespace App\Http\Controllers\Api\Requester;

class DeckManagerRequesterAPI extends APIRequester
{

    public static function getApiUrl(){
        return env("CARD_MANAGER_API");
    }

    public static function getSingleDeck(string $id)
    {
        return self::sendAPIRequest(self::getApiUrl() . "/decks/$id");
    }

    public static function getMultipleDecks(array $decks)
    {

        foreach ($decks as $key => $deck) {

            $api_deck = self::getSingleDeck($deck)->message;

            if (empty($api_deck->id)) {
                flash("Le pack $deck n'a pas été trouvé")->error();
                unset($decks[$key]);
            } else
                $decks[$key] = $api_deck;

        }

        return $decks;

    }

    public static function getRandomCard(string $deck, int $amount = 1)
    {

        $cards = self::sendAPIRequest(self::getApiUrl() . "/decks/$deck/responses");

        $selected_cards = [];

        for ($i = 0; $i < $amount; $i++)
            array_push($selected_cards, $cards[rand(0, count($cards) - 1)]);

        return $selected_cards;

    }

    public static function getAllDeckCards(string $deck)
    {

        return self::sendAPIRequest(self::getApiUrl() . "/decks/$deck/cards")->message;

    }

    public static function getResponsesCardsFromDeck(string $deck)
    {

        $cards = self::getAllDeckCards($deck);
        return $cards->responses;

    }

    public static function getCallsCardsFromDeck(string $deck)
    {

        $cards = self::getAllDeckCards($deck);
        return $cards->calls;

    }

}
