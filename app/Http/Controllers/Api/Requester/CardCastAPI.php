<?php


namespace App\Http\Controllers\Api\Requester;

class CardCastAPI
{

    public static string $api_url = "https://api.cardcastgame.com/v1";

    private static function sendAPIRequest(string $url)
    {

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL             => $url,
            CURLOPT_CONNECTTIMEOUT  => 3,
            CURLOPT_TIMEOUT         => 3,
            CURLOPT_RETURNTRANSFER  => true,
        ]);

        $response = curl_exec($ch);

        if (curl_errno($ch)) {

            abort_custom(501, trans("api.crash", ["data" => "API CardCastGame indisponible"]));

        }

        curl_close($ch);

        return json_decode($response);

    }

    public static function getSingleDeck(string $id)
    {
        return CardCastAPI::sendAPIRequest(CardCastAPI::$api_url . "/decks/$id");
    }

    public static function getMultipleDecks(array $decks)
    {

        foreach ($decks as $key => $deck) {

            $api_deck = CardCastAPI::getSingleDeck($deck);
            if (!empty($api_deck->id)) {
                flash("Le pack $deck n'a pas été trouvé")->error();
                unset($decks[$key]);
            } else
                $decks[$key] = $api_deck;

        }

        return $decks;

    }

    public static function getRandomCard(string $deck, int $amount = 1)
    {

        $cards = CardCastAPI::sendAPIRequest(CardCastAPI::$api_url . "/decks/$deck/responses");

        $selected_cards = [];

        for ($i = 0; $i < $amount; $i++)
            array_push($selected_cards, $cards[rand(0, count($cards) - 1)]);

        return $selected_cards;

    }

    public static function getAllDeckCards(string $deck)
    {

        $cards = CardCastAPI::sendAPIRequest(CardCastAPI::$api_url . "/decks/$deck/cards");

        foreach ($cards as $cat => $cat_content) {

            foreach ($cat_content as $cat_key => $card) {

                $current = $cards->$cat[$cat_key];

                unset($current->id);
                unset($current->created_at);
                unset($current->nsfw);

                $current = $current->text;

            }


        }

        return $cards;

    }

    public static function getResponsesCardsFromDeck(string $deck)
    {

        $cards = CardCastAPI::getAllDeckCards($deck);
        return $cards->responses;

    }

    public static function getCallsCardsFromDeck(string $deck)
    {

        $cards = CardCastAPI::getAllDeckCards($deck);
        return $cards->calls;

    }

}
