<?php


namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Redirect;

class HasUsername
{

    /**
     * @TODO Use cookie instead of session for username storing
     *
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next){

        if(!(session()->has('username'))){

            flash("Vous n'avez pas de nom d'utilisateur !");
            return Redirect::route('home');

        }

        return $next($request);

    }

}
