<?php


namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\DB;

class CheckApiToken
{

    public function handle(Request $request, Closure $next){

        if(!$request->has('token'))
            return false;

        if(DB::table('game_tokens')->where('token', '=', $request->get('token'))->get()->isEmpty())
            return false;

        return $next($request);

    }

}
