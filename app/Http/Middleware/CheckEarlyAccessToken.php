<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CheckEarlyAccessToken
{

    public function handle(Request $request, Closure $next){

        if((env("EARLY_ACCESS") == true) && (!session()->has("early_access_token") || (array_search(session()->get("early_access_token"), config("early_access_token")) === false)))
        {
            flash("Vous devez posséder un token d'accès anticipé pour utiliser la plateforme !")->warning();
            return Redirect::route("early-access");
        }

        return $next($request);

    }

}
