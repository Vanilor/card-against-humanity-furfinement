<?php

namespace App\Http\Objects;

class Cards {

    private array $fullSet;

    public function __construct()
    {
        $this->fullSet = $this->generateSet();
    }

    public function getNewSet(){
        return collect($this->fullSet);
    }

    public function setSet(array $set){

        if($this->validateSet($set))
            $this->fullSet = $set;
        else
            abort(501, "[ERREUR] Le jeu de carte entré est invalide");
    }

    private function validateSet(array $cards){

        foreach($cards as $card)
            if(!array_key_exists('type', $card) || !array_key_exists('value', $card))
                return false;

        return true;

    }

    public static function generateSet(){

        /*
         * 1 = Coeur
         * 2 = Carreau
         * 3 = Pic
         * 4 = Trèfle
        */
        $types = [1, 2, 3, 4];
        $values = ["2","3","4","5","6","7","8","9","10","V","D","R","A"];

        $fullSet = [];

        foreach($types as $type){
            foreach($values as $value) {

                array_push($fullSet, [
                    "type" => $type,
                    "value" => $value,
                ]);

            }
        }

        return $fullSet;

    }

}
