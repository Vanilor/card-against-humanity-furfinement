<?php


namespace App\Objects;

use Carbon\Carbon;
use Illuminate\Support\Str;

class Player
{

    public string $name;
    public int $score;
    private string $api_token;
    public bool $is_czard;
    public int $ident;
    public int $current_index;
    public array $current_hand;
    public array $responses_deck_shuffled;
    public Carbon $last_played;
    public Carbon $last_round_check;

    /**
     * Player constructor.
     *
     * @param string $name
     * @param int $score
     * @param bool $is_czard
     * @param int $current_index
     * @param array $current_hand
     * @param array $reponses_deck
     */
    public function __construct(string $name = "Player", int $score = 0,
                                bool $is_czard = false, int $current_index = 0,
                                array $current_hand = [], array $decks = [],
                                bool $create_new_player = false, string $api_token = NULL)
    {
        $this->name = $name;
        $this->score = $score;

        if ($api_token === NULL)
            $this->api_token = Str::random(32);
        else
            $this->api_token = $api_token;

        $this->is_czard = $is_czard;
        $this->current_index = $current_index;
        $this->current_hand = $current_hand;

        if($create_new_player === false){
            $this->responses_deck_shuffled = $decks;
        }

        else {

            $this->last_played = Carbon::now();
            $this->last_round_check = Carbon::now();
            $this->responses_deck_shuffled = [];

            foreach($decks as $deck_name => $deck)
                foreach ($deck as $deck_internal_key => $cardText)
                    array_push($this->responses_deck_shuffled, ["deck" => $deck_name, "key" => $deck_internal_key]);

        }

    }

    public function getApiToken()
    {
        return $this->api_token;
    }

    public function setApiToken(string $api_token){

        $this->api_token = $api_token;

    }

    public function setIdentifier(int $ident){
        $this->ident = $ident;
    }

    /**
     * Convert Player object to DB-insertable array
     *
     * @return array
     */
    public function toArray()
    {

        return [
            "name" => $this->name,
            "score" => $this->score,
            "is_czard" => $this->is_czard,
            "current_index" => $this->current_index,
            "current_hand" => $this->current_hand,
            "responses_deck_shuffled" => $this->responses_deck_shuffled
        ];

    }

    /**
     *
     * Convert Player object to JSON array
     *
     * @return string|false
     *
     */
    public function toJSON()
    {

        return json_encode($this->toArray());

    }

    public function shuffleDeck()
    {

        return shuffle($this->responses_deck_shuffled);

    }

    /**
     *
     * @param array $data
     * @return Player|bool
     *
     */
    public static function getPlayerByArray(array $data, string $api_token = NULL)
    {

        $properties = ["name", "score", "is_czard", "current_index", "current_hand", "responses_deck_shuffled"];
        foreach ($properties as $property)
            if (!array_key_exists($property, $data))
                return false;

        return new Player($data["name"], $data["score"],
            $data["is_czard"], $data["current_index"],
            $data["current_hand"], $data["responses_deck_shuffled"],
            false, $api_token);


    }


}
