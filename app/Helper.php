<?php

use Symfony\Component\HttpKernel\Exception\HttpException;

if(!function_exists("shuffle_assoc")){

    /**
     *
     * Shuffle an array with associative keys
     *
     * @param array $array
     * @return array
     *
     */
    function shuffle_assoc(array $array){

        if (!is_array($array)) return $array;

        $keys = array_keys($array);
        shuffle($keys);

        $random = [];

        foreach ($keys as $key)
            $random[$key] = $array[$key];

        return $random;

    }

}

if(!function_exists("abort_custom")){

    function abort_custom(int $code, $message, string $routeToRedirect = "home"){

        flash($message)->error();
        throw new HttpException(501, $message);

    }

}

if(!function_exists("app_name")){

    function app_name(){

        return str_replace("_", " ", env("APP_NAME"));

    }

}
