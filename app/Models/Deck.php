<?php


namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Deck extends Model
{

    protected $collection = "limite_limite_decks";
    protected $primaryKey = "id";

    protected $fillable = [
        "id", "responses", "calls"
    ];

    protected $hidden = [
        "_id",
    ];
}
