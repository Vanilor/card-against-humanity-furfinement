<?php

namespace App\Models;

use App\Objects\Player;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Jenssegers\Mongodb\Eloquent\Model;

/*
    * @TODO Optimize var generation by set class-related like players
    * @TODO Use the Player object far more
    * @TODO Implement user-defined in-game options
*/
class LimiteLimiteGame extends Model
{

    protected $collection = "limite_limite_sessions";

    protected $fillable = [
        "token", "name", "password", "host", "players", "status",
        "calls_decks", "czard_index", "current_round", "last_round",
        "responses_decks", "active", "restart_token", "ident_index",
    ];

    /*
     * Immutable options
     */
    public const RESPONSES_DECKS                = "responses_decks";
    public const CALLS_DECKS                    = "calls_decks";
    public const MIN_PLAYER_NB                  = 3;
    public const PLAYER_HAND_MAX_CARD_STRICT    = 12;   //Absolute max cards in player hand amount
    public const SECONDS_BEFORE_NEXT_ROUND      = 4;
    public const STATUS_GAME_OVER               = 0;
    public const STATUS_PLAYING                 = 1;

    /*
     * Default user-defined options
     */
    public const PLAYER_HAND_MAX_CARD           = 8;    //Max cards amount in player hand
    public const MAX_POINTS_AMOUNT              = 2;    //Maximum reachable score. Need to be setup in options later
    public const TIMEOUT_SECONDS                = 300;  //Five minutes timeout (hasn't played cards for five minutes)
    public const KICK_PLAYER_SECONDS            = 60;   //If no request to API for TIMEOUT_DROP_PLAYER_SECONDS seconds, drop the player from game

    /*
     * Identifiers
     */
    public const CALL_CARD_PLACEHOLDER          = "<span class=\"call-card-placeholder\"></span>";


    /*
     * ====================================================
     *              DIRECT GAME-RELATED FUNCTIONS
     * ====================================================
     */

    /**
     * Check if the game has an empty password
     *
     * @return bool
     */
    public function hasPassword(){

        return !Hash::check("", $this->password);

    }

    /**
     * Add a player to the current game
     *
     * @param Player $player
     * @return bool
     */
    public function addPlayer(Player $player){

        $player->setIdentifier(++$this->attributes["ident_index"]);
        $this->attributes["players"][$player->getApiToken()] = $player;
        $this->save();

        return true;

    }

    /**
     * Checks if a username already exists
     *
     * @param string $username
     * @return bool
     */
    public function doesUsernameExist(string $username){

        foreach ($this->attributes["players"] as $player)
            if($player['name'] === $username)
                return true;

        return false;

    }


    /**
     * Get all response decks
     *
     * @return mixed
     */
    public function getResponsesDecks()
    {
        return $this->responses_decks;
    }

    /**
     * Checks if they are enough players to start the game
     *
     * @return int
     */
    public function areMissingPlayers()
    {

        $players_nb = count($this->getActivePlayers());

        if ($players_nb < self::MIN_PLAYER_NB)
            return self::MIN_PLAYER_NB - $players_nb;

        return 0;

    }

    /**
     * Get the players that hadn't filled the round
     *
     * @return array
     */
    public function getHadntPlayPlayers(){

        $players = $this->getPlayers();
        $blanks = $this->getBlanksInCall();

        $missing_players = [];

        foreach($players as $api_token => $player){

            if($player["is_czard"])
                continue;

            if(!$this->hasPlayed($api_token))
                array_push($missing_players, $player["name"]);

            else if(count($this->attributes["current_round"]["response_cards"][$api_token]["cards"]) != $blanks)
                array_push($missing_players, $player["name"]);

        }

        return $missing_players;

    }

    /**
     * Retrieve the current round czard list
     *
     * @return array
     */
    public function getCurrentCzards(){

        $players = $this->getPlayers();
        $czards = [];

        foreach($players as $api_token => $player)
            if($player["is_czard"] == true)
                array_push($czards, $player["name"]);

        return $czards;

    }

    /**
     * Retrieve all in-game players
     *
     * @return array
     */
    public function getPlayers(){

        $players = [];

        foreach($this->attributes["players"] as $api_token => $player){

            $players[$api_token] = $player;
            unset($players[$api_token]["responses_hand_shuffled"]);

        }

        return $players;

    }

    /**
     * Increment the call card index, going to the next call card
     */
    public function incrementCallIndex()
    {
        $this->attributes["current_round"]["call_cards"]["current_index"]++;
        $this->save();
    }

    public function generateCallCard()
    {
        //Generate the current call card text
        if (empty($this->attributes["current_round"]["call_cards"]["cards"]))
            return "";

        $strArray = $this->getCardTextFromDescriptor($this->getCurrentCallCard(), self::CALLS_DECKS);

        $str = "";
        foreach ($strArray as $key => $txt) {

            $str .= $txt;
            if (array_key_last($strArray) != $key)
                $str .= self::CALL_CARD_PLACEHOLDER;

        }

        return $str;

    }

    public function getBlanksInCall(){

        return count($this->getCardTextFromDescriptor($this->getCurrentCallCard(), self::CALLS_DECKS)) - 1;

    }

    public function countPlayedCards(){

        $counter = 0;
        foreach($this->attributes["current_round"]["response_cards"] as $player)
            foreach($player["cards"] as $card)
                $counter++;

        return $counter;
    }

    public function getCurrentCallCard()
    {

        return $this->attributes["current_round"]["call_cards"]["cards"][
        $this->attributes["current_round"]["call_cards"]["current_index"]
        ];

    }

    public function getAllPlayedCards(bool $identify_players = false)
    {

        $cards = [];
        $playerResponseIndex = 0;

        foreach ($this->attributes['current_round']['response_cards'] as $player_api_token => $player_round) {

            $player = $this->getPlayer($player_api_token);

            $data = [];
            if($identify_players){
                $data["name"] = $player["name"];
                $data["ident"] = $player["ident"];
            }
            $data["response_id"] = $playerResponseIndex;
            $data["cards"] = [];

            foreach ($player_round["cards"] as $cardIndex){

                array_push($data["cards"], [
                    "deck" => $player["responses_deck_shuffled"][$cardIndex]["deck"],
                    "text" => $this->getCardTextFromDescriptor($player["responses_deck_shuffled"][$cardIndex]),
                ]);

            }

            array_push($cards, $data);

            $playerResponseIndex++;
        }

        return $cards;

    }

    public function isRoundOver()
    {

        $required_cards = $this->getBlanksInCall()*(count($this->attributes['players']) - 1); //Get all players count except the Czard
        $current_cards = 0;
        $timeouts = $this->getTimeoutedPlayers();

        //If some players has timed out, reduce the number of required cards
        foreach ($timeouts as $timeout)
            $required_cards -= $this->getBlanksInCall();

        foreach($this->attributes["current_round"]["response_cards"] as $player_api_token => $player)
            foreach($player["cards"] as $card)
                $current_cards++;

        if ($current_cards === $required_cards)
            return true;

        return false;

    }

    public function getWinner(bool $getAPIToken = false){

        $responses = $this->attributes["current_round"]["response_cards"];
        foreach ($responses as $api_token => $response){

            if($response["is_winner"] === 1){

                if($getAPIToken)
                    return $api_token;
                else
                    return $this->getPlayer($api_token)["ident"];

            }
        }

        return false;

    }

    public function startNewRound(){

        $players = $this->getPlayers();

        //Send new cards to players
        foreach($players as $api_token => $player){

            $players[$api_token]["current_index"]++;
            $this->addCardToPlayerHand($api_token, $players[$api_token]["current_index"]);

        }

        $this->attributes["players"] = $players;

        //Purge current round responses
        $this->attributes["current_round"]["response_cards"] = [];
        $this->save();

    }

    public function saveCurrentRound(){

        $currentRound = $this->attributes["current_round"];

        $this->attributes["last_round"] = [];
        $this->attributes["last_round"]["players"] = [];
        $this->attributes["last_round"]["call_card"] = $this->generateCallCard();

        $players = $this->getPlayers();

        foreach($players as $api_token => $player){

            $cardsDescriptors = $this->getPlayedCardByPlayer($api_token, true);
            $cards = [];

            foreach($cardsDescriptors as $descriptor) {

                $descriptor = $descriptor[0];

                array_push($cards, [

                    "deck" => $descriptor["deck"],
                    "text" => $this->attributes["responses_decks"][$descriptor["deck"]][$descriptor["key"]]["text"][0],

                ]);
            }


            array_push($this->attributes["last_round"]["players"], [
                "name" => $player["name"],
                "ident" => $player['ident'],
                "score" => $player["score"],
                "cards" => $cards,
                "hasTimedout" => $this->hasTimedout($api_token),
                "is_czard" => $player["is_czard"],
                "is_host" => $this->isHost($api_token),
                "is_winner" => !$player["is_czard"] ? $currentRound["response_cards"][$api_token]["is_winner"] : 0,
            ]);

        }

        $this->save();

    }

    public function hasCzard(){

        foreach ($this->getPlayers() as $player)
            if($player["is_czard"])
                return true;

        return false;

    }

    public function getPlayersStatus(string $actual_player_api_token){

        //Retrieve current in-game players data
        $dbPlayers = $this->getPlayers();
        $sendPlayers = [];

        foreach ($dbPlayers as $api_token => $player){

            //Purge sensitive information
            array_push($sendPlayers, [
                "name"          => ($api_token === $actual_player_api_token) ? $player["name"]." (You)" : $player["name"],
                "status"        => [
                    "timedout"      => $this->hasTimedout($api_token),
                    "is_host"       => $this->isHost($api_token),
                    "is_czard"      => $this->isPlayerCzard($api_token),
                    "is_playing"    => $this->hasPlayed($api_token),
                ],
                "score"         => $player["score"],
                "ident"         => $player["ident"],

            ]);

        }

        return $sendPlayers;

    }

    public function getTimeoutedPlayers(){

        $timeouts = [];

        foreach ($this->getPlayers() as $api_token => $player)
            if($this->hasTimedout($api_token) && !$this->isPlayerCzard($api_token))
                array_push($timeouts, $api_token);

        return $timeouts;
    }

    public function getActivePlayers(){

        return array_diff(array_keys($this->attributes["players"]), $this->getTimeoutedPlayers());

    }

    public function isGameOver(){

        foreach($this->getPlayers() as $player)
            if($player["score"] === self::MAX_POINTS_AMOUNT)
                return $player["ident"];

        return false;
    }

    /*
    * @TODO Improve this
    */
    public function setRoundWinner(int $responses_array_index){

        $responses = $this->attributes["current_round"]["response_cards"];

        //There is an error somewhere, aborting. Maybe try an automatic fix ? Need more data about error causes
        if(count($responses) < $responses_array_index)
            return false;

        $i = 0;
        $winner_key = "";
        foreach($responses as $key => $response){

            if($i === $responses_array_index){
                $winner_key = $key;
                break;
            }

            $i++;
        }

        $this->attributes["current_round"]["response_cards"][$winner_key]["is_winner"] = 1;
        $this->save();

        return $winner_key;

    }

    public function getRestartToken(){

        return $this->attributes["restart_token"];

    }

    public function regenerateRestartToken(){

        $this->attributes["restart_token"] = Str::random(16);
        $this->save();

    }

    public function purgeDeadPlayers(){

        $players = $this->getPlayers();

        foreach($players as $api_token => $player)
            if(!$this->isAlive($api_token))
                $this->removePlayer($api_token);

    }

    public function restartGame(){

        $players = $this->getPlayers();
        $this->attributes["ident_index"] = 0;

        foreach ($players as $api_token => $player){

            $newData = new Player($player["name"], 0, false,
                0, [], $this->getResponsesDecks(), true, $api_token);
            $newData->shuffleDeck();
            $newData->setIdentifier($this->attributes["ident_index"]++);

            $players[$api_token] = $newData;

        }

        $this->attributes["players"]            = $players;
        $this->attributes["current_round"]      = $this->generateCurrentRound($this->attributes["calls_decks"]);
        $this->attributes["last_round"]         = [];
        $this->attributes["czard_index"]        = 0;

        $this->save();
    }

    public function electRandomHost(string $actual_host){

        $electables = array_keys($this->attributes["players"]);
        $electables = array_diff($electables, [$actual_host]);

        $this->attributes["host"] = $electables[array_rand($electables, 1)];
        $this->save();

    }

    public function generateCurrentRound($call_decks){


        $arr = [];
        $arr["response_cards"] = [];
        $arr["call_cards"] = [];
        $arr["call_cards"]["cards"] = [];
        $arr["call_cards"]["current_index"] = 0;

        foreach ($call_decks as $deck_name => $deck)
            foreach ($deck as $deck_internal_key => $cardText)
                array_push($arr["call_cards"]["cards"], ["deck" => $deck_name, "key" => $deck_internal_key]);

        //Shuffling call cards to get randomness
        shuffle($arr["call_cards"]["cards"]);

        return $arr;

    }

    public function generateLastRound(){

        $output = [];
        $output["call_card"] = "";
        $output["players"] = [];

        return $output;

    }

    public function getLastRound(){

        $data = $this->attributes["last_round"];

        if(empty($data))
            return $this->generateLastRound();

        $output = [];
        $output["call_card"] = $data["call_card"];
        $output["players"] = [];

        foreach($data["players"] as $player){

            array_push($output["players"], [
                "name" => $player["name"],
                "is_winner" => (bool)$player["is_winner"],
                "ident" => $player["ident"],
                "score" => $player["score"],
                "cards" => $player["cards"],
            ]);
        }


        return $output;

    }

    public function getDefaultOptions(){

        $options = [];
        $options["max_points"]              = self::MAX_POINTS_AMOUNT;
        $options["max_player_hand_cards"]   = self::PLAYER_HAND_MAX_CARD;
        $options["timeout_seconds"]         = self::TIMEOUT_SECONDS;
        $options["kick_seconds"]            = self::KICK_PLAYER_SECONDS;

        return $options;

    }

    public function removePlayer(string $api_token){

        //The host can't be removed from the game
        if($this->isHost($api_token))
            return;

        if(isset($this->attributes["current_round"]["response_cards"][$api_token]))
            unset($this->attributes["current_round"]["response_cards"][$api_token]);

        unset($this->attributes["players"][$api_token]);

        $this->save();

    }

    public function setCzardChoosenTime(){

        $this->attributes["current_round"]["czard_choosen_time"] = Carbon::now()->toDateTimeString();
        $this->save();

    }

    public function getCzardChoosenTime(){

        return $this->attributes["current_round"]["czard_choosen_time"] ?? Carbon::now()->toDateTimeString();

    }

    public function getOption(string $name){

        //@TODO Remove this patch for old games
        if(!isset($this->attributes["options"]))
            return "ERROR_OPTIONS_NOT_SET";

        return (!empty($name)) ? ($this->attributes["options"][$name] ?? $this->getOptionError($name)) : $this->attributes["options"];

    }

    public function getTags(){

        $tags = $this->getOption("tags");

        return (is_array($tags) ? implode(", ", $tags) : $this->getOptionError("tags"));


    }

    private function getOptionError(string $name){

        return "ERROR_OPTIONS_OPTION_".strtoupper("$name")."_NOT_DEFINED";

    }

    //@TODO
    public function setOptions(string $name){



    }

    public function countPlayers(){

        return count($this->attributes["players"]);

    }

    //@TODO Implement Spectate feature
    public function countSpectators(){

        return 0;

    }

    public function getHost(){

        $players = $this->getPlayers();
        foreach ($players as $api_token => $player)
            if($this->isHost($api_token))
                return Player::getPlayerByArray($player);

            //@TODO Return custom Exception
            return NULL;
    }

    public function getStatus(){

        if ($this->isGameOver() !== false)
            return self::STATUS_GAME_OVER;

        return self::STATUS_PLAYING;

    }

    /**
     * Set the current game status
     *
     * @param int $status
     */
    public function setGameStatus(int $status){

        $this->attributes["status"] = $status;
        $this->save();

    }










    /*
     * ====================================================
     *              PLAYER-RELATED FUNCTIONS
     * ====================================================
     *
     * @TODO Move those to Player object
     */

    /**
     * Get the player identified by $api_token token
     *
     * @param string $api_token
     * @return mixed
     *
     */
    public function getPlayer(string $api_token)
    {

        if (isset($this->attributes['players'][$api_token]))
            return $this->attributes['players'][$api_token];

        else
            return NULL;

    }

    /**
     * Check if the requested player hand is empty
     *
     * @param string $api_token
     * @return bool
     */
    public function isPlayerHandEmpty(string $api_token)
    {

        if (empty($this->getPlayer($api_token)['current_hand']))
            return true;

        return false;

    }

    /**
     * Get the requested player current hand
     *
     * @param string $api_token
     * @return array
     */
    public function getPlayerHand(string $api_token)
    {

        $hand = $this->getPlayer($api_token)['current_hand'];
        $cards = [];

        foreach ($hand as $card_id) {

            array_push($cards, [
                "id" => $card_id,
                "text" => $this->getCardTextFromDescriptor($this->getPlayer($api_token)['responses_deck_shuffled'][$card_id]),
            ]);

        }

        return $cards;

    }

    public function getPlayerCurrentResponseIndex(string $api_token){

        return $this->attributes["players"][$api_token]["current_index"];

    }



    /**
     * Add a card from player identified by $api_token in the current round
     * Generates the player current round array if he has not played
     *
     * @param string $api_token
     * @param int $index
     * @return bool
     */
    public function addCardToCurrentRound(string $api_token, int $index)
    {

        if(!isset($this->attributes["current_round"]["response_cards"][$api_token])){

            $this->attributes["current_round"]["response_cards"][$api_token] = [];
            $this->attributes["current_round"]["response_cards"][$api_token]["cards"] = [];
            $this->attributes["current_round"]["response_cards"][$api_token]["is_winner"] = 0; //We use a int in order to add some bonus points to well-played players. Maybe an extra feature ?

        }

        array_push($this->attributes["current_round"]["response_cards"][$api_token]["cards"], $index);
        $this->save();

        return true;

    }

    public function getCardDescriptorFromIndex(string $api_token, int $index){

        return $this->attributes["players"][$api_token]["responses_deck_shuffled"][$index];

    }

    /**
     * Remove the played cards by player identified by $api_token from current round
     *
     * @param string $api_token
     * @return bool true if everything's correct | false if player hadn't play
     */
    public function removeCardsFromCurrentRound(string $api_token){

        if(!isset($this->attributes["current_round"]["response_cards"][$api_token]))
            return false;

        //Free the played cards array by player
        $this->attributes["current_round"]["response_cards"][$api_token]["cards"] = [];
        $this->save();

        return true;

    }

    /**
     * Increments the player responses index, then the next card will be insert in it's deck if necessary
     *
     * @param string $api_token
     * @return bool
     *
     */
    public function incrementPlayerIndex(string $api_token)
    {

        $this->attributes["players"][$api_token]["current_index"]++;
        $this->save();

        return true;
    }

    /**
     * Reset the player card index if it has reached the maximum card array length
     *
     * @param string $api_token
     * @return bool
     */
    public function resetPlayerIndexIfNecessary(string $api_token)
    {

        $cards_nb = count($this->getPlayer($api_token)['responses_deck_shuffled']);

        if ($this->attributes["players"][$api_token]["current_index"] >= $cards_nb) {

            $this->attributes["players"][$api_token]["current_index"] = 0;
            $this->save();

            return true;
        }

        return false;

    }

    /**
     * Add the card_id, corresponding to the response card array index, to the player hand
     *
     * @param string $api_token
     * @param int $card_id
     * @return bool
     */
    public function addCardToPlayerHand(string $api_token, int $card_id)
    {

        array_push($this->attributes["players"][$api_token]["current_hand"], $card_id);
        $this->save();

        return true;

    }

    /**
     * Remove the card_id, corresponding to, from the player hand
     *
     * @param string $api_token
     * @param int $card_id
     * @return bool
     */
    public function removeCardFromPlayerHand(string $api_token, int $card_id)
    {

        $card_index = $this->doesOwnCard($api_token, $card_id);
        if ($card_index === false)
            return false;

        unset($this->attributes['players'][$api_token]["current_hand"][$card_index]);
        $this->save();
        return true;

    }

    public function doesOwnCard(string $api_token, int $card_id){

        return array_search($card_id, $this->attributes['players'][$api_token]["current_hand"]);

    }

    public function getCardTextFromDescriptor(array $desc, $type = self::RESPONSES_DECKS)
    {

        if (count($desc) != 2 || !isset($desc["deck"]) || !isset($desc["key"]))
            return NULL;


        if ($type === self::RESPONSES_DECKS)
            return $this->$type[$desc["deck"]][$desc["key"]]["text"][0];

        elseif ($type === self::CALLS_DECKS)
            return $this->$type[$desc["deck"]][$desc["key"]]["text"];

        else
            return NULL;

    }

    public function shufflePlayerDeck(string $api_token)
    {

        shuffle($this->attributes['players'][$api_token]["responses_deck_shuffled"]);
        $this->save();

        return true;

    }


    public function countPlayedCardsByPlayer(string $api_token){

        //Avoid error in case the player hadn't play at all : key $api_token doesn't currently exists
        if(!isset($this->attributes["current_round"]["response_cards"][$api_token]))
            return 0;

        return count($this->attributes["current_round"]["response_cards"][$api_token]["cards"]);

    }

    public function getPlayerMissingCardsNb(string $api_token)
    {

        //Player hasn't played
        if(isset($this->attributes["current_round"]["response_cards"][$api_token]))
            $current_hand_cards_nb = count($this->getPlayer($api_token)["current_hand"]) + count($this->attributes["current_round"]["response_cards"][$api_token]["cards"]);

        else
            $current_hand_cards_nb = count($this->getPlayer($api_token)["current_hand"]);


        return self::PLAYER_HAND_MAX_CARD - $current_hand_cards_nb;

    }

    public function hasEverPlayedDuringRound(string $api_token){

        if (isset($this->attributes["current_round"]["response_cards"][$api_token]))
            return true;

        return false;

    }

    public function hasPlayed(string $api_token)
    {

        if ($this->hasEverPlayedDuringRound($api_token) &&
            (count($this->attributes["current_round"]["response_cards"][$api_token]["cards"]) === $this->getBlanksInCall()))
        {
            return true;
        }

        return false;

    }

    public function hasEnoughCardsInGame(string $api_token)
    {

        if($this->getPlayerMissingCardsNb($api_token) > 0)
            return false;

        return true;

    }

    public function isPlayerCzard(string $api_token)
    {

        if ($this->getPlayer($api_token)["is_czard"])
            return true;

        return false;

    }

    public function getCardTextFromPlayerHandById(string $api_token, $id_in_hand)
    {

        $card_index = array_search($id_in_hand, $this->attributes['players'][$api_token]["current_hand"]);
        if ($card_index === false)
            return false;

        return $this->getCardTextFromDescriptor(
            $this->attributes["players"][$api_token]["responses_deck_shuffled"][
                $this->attributes['players'][$api_token]["current_hand"][$card_index]
            ]
        );

    }

    public function getPlayedCardByPlayer(string $api_token, bool $descriptorsOnly = false)
    {

        if (!isset($this->attributes["current_round"]["response_cards"][$api_token]) ||
            empty($this->attributes["current_round"]["response_cards"][$api_token]["cards"]))
        {
            return [];
        }

        $cards = $this->attributes["current_round"]["response_cards"][$api_token]["cards"];
        $result = [];

        foreach($cards as $card){

            if($descriptorsOnly){
                array_push($result, [
                    $this->attributes["players"][$api_token]["responses_deck_shuffled"][$card]
                ]);
            }
            else {

                array_push($result, [
                    "id_in_hand" => $card,
                    "card" => [
                        "id" => $card,
                        "text" => $this->getCardTextFromDescriptor($this->attributes["players"][$api_token]["responses_deck_shuffled"][$card]),
                    ]

                ]);

            }

        }

        return $result;

    }

    public function getPlayerLastPlayedCardId(string $api_token){

        if(!$this->hasPlayed($api_token))
            return false;

        return $this->attributes["current_round"]["response_cards"][$api_token]["cards"][
            array_key_last($this->attributes["current_round"]["response_cards"][$api_token]["cards"])
        ];

    }

    public function hasCardInHand(string $api_token, int $response_deck_index){

        if(array_search($response_deck_index, $this->attributes["players"][$api_token]["current_hand"]) !== false)
            return true;

        return false;

    }

    public function incrementPlayerScore(string $api_token){

        $this->attributes["players"][$api_token]["score"]++;
        $this->save();

    }

    public function electNewCzard(string $actual_czard_api_token, bool $randomElection = false){

        //If there's no czard actually
        if($actual_czard_api_token === ""){

            if($randomElection)
                $this->attributes["players"][array_rand($this->attributes["players"], 1)]["is_czard"] = true;
            else
                $this->attributes["players"][array_key_first($this->attributes["players"])]["is_czard"] = true;

            $this->save();

            return;
        }

        //Unset the actual czard
        $this->attributes["players"][$actual_czard_api_token]["is_czard"] = false;
        $new_czard_key = array_key_first($this->attributes["players"]); //Set a default value

        if($randomElection){

            $electables = array_keys($this->attributes["players"]);
            $electables = array_diff($electables, [$actual_czard_api_token]);

            $new_czard_key = $electables[array_rand($electables, 1)];
        }
        else {

            if(!isset($this->attributes["czard_index"]))
                $this->attributes["czard_index"] = -1;

            $new_czard_index = $this->attributes["czard_index"]+1;

            $this->attributes["czard_index"] = $new_czard_index % count($this->attributes["players"]);

            $i = 0;
            $players = $this->getPlayers();
            foreach($players as $api_token => $player) {
                if($i === $new_czard_index){
                    $new_czard_key = $api_token;
                    break;
                }
                $i++;
            }

        }

        $this->attributes["players"][$new_czard_key]["is_czard"] = true;

        $this->save();
    }

    public function setLastActivity(string $api_token){

        $this->attributes["players"][$api_token]["last_played"] = Carbon::now()->toDateTimeString("second");
        $this->save();

    }

    public function getLastActivity(string $api_token){

        if(!isset($this->attributes["players"][$api_token]["last_played"]))
            $this->setLastActivity($api_token);

        return Carbon::make($this->attributes["players"][$api_token]["last_played"]);

    }

    public function hasTimedout(string $api_token){

        if((Carbon::now()->diffInSeconds($this->getLastActivity($api_token)) >= self::TIMEOUT_SECONDS) &&
            !$this->hasPlayed($api_token) && $this->isPlayerCzard($api_token))
            return true;

        return false;

    }

    //Alive is different from active : Active means that player is still in the game but AFK
    //Player not alive means that player is no longer in the game, so drop him from
    public function setAlive(string $api_token){

        $this->attributes["players"][$api_token]["last_round_check"] = Carbon::now()->toDateTimeString("seconds");
        $this->save();

    }

    public function isAlive(string $api_token){

        if(Carbon::now()->diffInSeconds(Carbon::make($this->attributes["players"][$api_token]["last_round_check"])) >= self::KICK_PLAYER_SECONDS
           && ($this->getWinner() !== false))
        {
            return false;
        }

        return true;

    }

    public function isHost(string $api_token){

        if($this->attributes["host"] === $api_token)
            return true;

        return false;

    }

}
